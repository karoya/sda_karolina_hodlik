//
//  main.cpp
//  Kabala
//
//  Created by Karolina Hodlik on 07/05/2017.
//  Copyright © 2017 Karolina Hodlik. All rights reserved.
//

#include <iostream>
#include <string>


int main() {
      std::string slowo;
    
    const char litery[23] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'k', 'l', 'm', 'n', 'o', 'p','q', 'r', 's', 't', 'v', 'x', 'y', 'z' };
    const int liczby[23] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500};
    
    do {
        std::cin >> slowo;
    } while ((slowo.length() > 25) || (slowo.length() == 0));
  
    int pomocnicza = 0;
    for (unsigned int i = 0; i < slowo.length(); i++)
    {
        for (int z = 0; z < 23; z++)
        {
            if (slowo[i] == litery[z])
            {
                pomocnicza += liczby[z];

            }
            
        }
    }
    std::cout<<pomocnicza<<std::endl;
    
    return 0;
}
