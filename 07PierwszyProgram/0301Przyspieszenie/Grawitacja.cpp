#include "Grawitacja.hpp"
#include "Obliczenia.hpp"

float przyspieszenieGrawitacyjne(float M, float R)
{
	return (G * M) / kwadrat(R);
}