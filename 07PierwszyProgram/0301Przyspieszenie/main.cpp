#include <iostream>
#include "Grawitacja.hpp"
#include "Obliczenia.hpp"

int main()
{
	std::cout << "kwadrat = " << kwadrat(3.3) << std::endl;
	std::cout << "grawitacja" << przyspieszenieGrawitacyjne(3.4, 8.9) << std::endl;

	return 0;
}