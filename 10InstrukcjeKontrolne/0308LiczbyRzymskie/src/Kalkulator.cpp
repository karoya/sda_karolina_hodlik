#include <iostream>

#include <string>

int main(int argc, char* argv[]) {
	std::string rzym;

	std::cout << "Podaj liczbe jak prawdziwy rzymianin: " << std::flush;

	std::getline(std::cin, rzym);

	if (rzym.empty())

	{

		std::cout << "Koniec" << std::endl;

		return 0;

	}

	int wynik = 0;

	for (unsigned i = 0; i < rzym.length(); ++i)

	{

		char znak = rzym[i];

		char nastepnyZnak = rzym[i + 1];

		if (i + 1 < rzym.length())

		{

			nastepnyZnak = rzym[i + 1];

		}

		else

		{

			nastepnyZnak = '\0';

		}

		if (znak == 'I')

		{

			wynik = wynik + 1;

			if (nastepnyZnak == 'V')

			{
				wynik = wynik - 2;
			} else if (nastepnyZnak == 'X') {
				wynik = wynik - 2;

			}

			else if (nastepnyZnak == 'L')

			{

				wynik = wynik - 2;

			}

			else if (nastepnyZnak == 'C')

			{

				wynik = wynik - 2;

			}

			else if (nastepnyZnak == 'D')

			{

				wynik = wynik - 2;

			}

			else if (nastepnyZnak == 'M')

			{

				wynik = wynik - 2;

			}

		}

		else if (znak == 'V')

		{

			wynik = wynik + 5;

		}

		else if (znak == 'X')

		{

			wynik = wynik + 10;

			if (nastepnyZnak == 'L')

			{

				wynik = wynik - 20;

			}

			else if (nastepnyZnak == 'C')

			{

				wynik = wynik - 20;

			}

			else if (nastepnyZnak == 'D')

			{

				wynik = wynik - 20;

			}

			else if (nastepnyZnak == 'M')

			{

				wynik = wynik - 20;

			}

		}

		else if (znak == 'L')

		{

			wynik = wynik + 50;

		}

		else if (znak == 'C')

		{

			wynik = wynik + 100;

			if (nastepnyZnak == 'D')

			{

				wynik = wynik - 200;

			}

			else if (nastepnyZnak == 'M')

			{

				wynik = wynik - 200;

			}

		}

		else if (znak == 'D')

		{

			wynik = wynik + 500;

		}

		else if (znak == 'M')

		{
			wynik = wynik + 1000;
		}
	}

	std::cout << wynik << std::endl;

	return 0;
