#include <iostream>
#include <string>

const std::string RPN[] =
{ "3 2 + 4 -", "1 1 1 1 + + +", "5 1 2 + 4 * + 3 -",
    "1 1 + 2 + 3 1 1 + 7 - 9 / * -" };
const int wyniki[] =
{ 1, 4, 14, 4 };

int wierzcholek;
int dane[256];

void wstaw(char znak)
{
		dane[wierzcholek] = znak - '0';
		wierzcholek += 1;
}

int wylicz(const std::string wyrazenie)
{
	wierzcholek = 0;
    for (unsigned i = 0; i < wyrazenie.length(); i++){

        if (wyrazenie[i] == ' ')
            continue;

        else if (wyrazenie[i] >= '0' && wyrazenie[i] <= '9')
        {
            wstaw(wyrazenie[i]);
        }

        else
            switch (wyrazenie[i])
        {
            case '+':
                dane[wierzcholek - 2] += dane[wierzcholek - 1];
                wierzcholek -= 1;
                break;
            case '-':
                dane[wierzcholek - 2] -= dane[wierzcholek - 1];
                wierzcholek -= 1;
                break;
            case '*':
                dane[wierzcholek - 2] *= dane[wierzcholek - 1];
                wierzcholek -= 1;
                break;
            case '/':
                dane[wierzcholek - 2] /= dane[wierzcholek - 1];
                wierzcholek -= 1;
                break;
            default:
                break;

        }

    }

    return dane[0];
}

void sprawdz(const std::string rpn, int oczekiwanyWynik)
{
    int wynik = wylicz(rpn);
    if (wynik == oczekiwanyWynik)
    {
        std::cout << "Cacy ";
    }
    else
    {
        std::cout << "Be   ";
    }
    std::cout << rpn << " => " << wynik << std::endl;
}

int main(int argc, char* argv[])
{
    for (int i = 0; i < 4; ++i)
    {
        sprawdz(RPN[i], wyniki[i]);
    }

    return 0;
}
