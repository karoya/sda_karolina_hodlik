#include <iostream>
#include <cmath>
#include <iomanip>

struct Vector3D
{
	double x, y, z;
};

Vector3D nowyWektor(double x, double y, double z)
{
	return Vector3D { x, y, z };
}

double dlugoscWektora (Vector3D a)
{
	return std::sqrt( (a.x*a.x) + (a.y*a.y) + (a.z*a.z) );

}

Vector3D normalizujWektor(Vector3D a)
{
	double dlugosc = dlugoscWektora(a);
	return nowyWektor(a.x/dlugosc, a.y/dlugosc, a.z/dlugosc);
}

Vector3D operator+ (Vector3D a, Vector3D b)
{
	return nowyWektor(a.x + b.x, a.y + b.y, a.z + b.z);
}

Vector3D operator- (Vector3D a, Vector3D b)
{
	return nowyWektor(a.x - b.x, a.y - b.y, a.z - b.z);
}

Vector3D operator* (Vector3D a, int liczba)
{
	return nowyWektor(a.x * liczba, a.y * liczba, a.z * liczba);
}

Vector3D operator* (int liczba, Vector3D a)
{
	return nowyWektor(liczba * a.x, liczba * a.y, liczba * a.z);
}

Vector3D operator- (Vector3D a)
{
	return nowyWektor(-a.x, -a.y, -a.z);
}

Vector3D operator+ (Vector3D a)
{
	return nowyWektor(a.x +(-a.x), a.y +(-a.y), a.z +(-a.z));
}

Vector3D operator* (Vector3D a, Vector3D b)
{
	return nowyWektor( (a.y*b.z)-(a.z*b.y),(a.z*b.x)-(a.x*b.z),(a.x*b.y)-(a.y*b.x));
}

double operator^ (Vector3D a, Vector3D b)
{
	return int(a.x*b.x + a.y*b.y + a.z*b.z);
}

std::ostream& operator<<(std::ostream& out, const Vector3D& v)
{
	std::streamsize precyzja = out.precision();  // zapisuj� star� precyzj�
	std::ios_base::fmtflags flagi = out.flags(); // zapisuj� stan flag
	out << std::setprecision(4) << std::fixed << '[' << v.x << ", " << v.y
			<< ", " << v.z << ']';
	out.flags(flagi);        // odtworzenie zapisanych flag
	out.precision(precyzja); // odtworzenie zapisanej precyzji
	return out;
}

// https://pl.wikipedia.org/wiki/Wektor
int main(int argc, char* argv[])
{
	Vector3D v = nowyWektor(1.0, 2.0, 3.0);
	Vector3D u = nowyWektor(4.0, 5.0, 6.0);
	std::cout << "v = " << v << ",\nu = " << u << std::endl;

	Vector3D vn = normalizujWektor(v);
	Vector3D un = normalizujWektor(u);
	std::cout << "Znormalizowane:\nv = " << vn << ",\nu = " << un << std::endl;

	Vector3D w;
	double s = 0.0;

	w = 3 * v;
	std::cout << "Mno�enie przez skalar #1: " << w << std::endl;

	w = v * 3;
	std::cout << "Mno�enie przez skalar #2: " << w << std::endl;

	w = -v;
	std::cout << "Wektor przeciwny: " << w << std::endl;

	w = v + u;
	std::cout << "Suma: " << w << std::endl;

	w = v + (-v);
	std::cout << "Suma z wektorem przeciwnym: " << w << std::endl;

	w = v - u;
	std::cout << "R�nica: " << w << std::endl;

	w = v * u;
	std::cout << "Iloczyn wektorowy: " << w << std::endl;

	w = normalizujWektor(v * u);
	std::cout << "Iloczyn wektorowy znormalizowany: " << w << std::endl;

	w = normalizujWektor(vn * un);
	std::cout << "Znormalizowany iloczyn wektor�w znormalizowanych: " << w << std::endl;

	s = v ^ u;
	std::cout << "Iloczyn skalarny: " << s << std::endl;

	return 0;
}
