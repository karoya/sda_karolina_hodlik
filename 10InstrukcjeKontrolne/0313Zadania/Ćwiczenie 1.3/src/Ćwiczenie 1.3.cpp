//============================================================================
// Name        : 3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>

void FizzBuzz(std::string& tablica)
{
	for (int i = 0; i <= 20; ++i)
	{
		if (i % 3 == 0)
		{
			std::cout << "Fizz" << " " << std::flush;
		}
		else if (i % 5 == 0)
		{
			std::cout << "Buzz" << " " << std:: flush;
		}

		else
		{
			std::cout << i << " " << std::flush;
		}
	}
}

int main() {

	std::string tablica;

	FizzBuzz(tablica);

	return 0;
}
