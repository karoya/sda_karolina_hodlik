//============================================================================
// Name        : 5.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//============================================================================
// Name        : 2.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
void sredniaArytmetyczna(int rozmiar, int* tablica)
{
	for (int i = 0; i < rozmiar; ++i)
	{
		std::cout << "Podaj liczb� do zbioru " << std::flush;
		std::cin >> tablica[i];
	}

	double wynik = 0;
	for (int i = 0; i < rozmiar; ++i)
	{
		wynik += tablica[i];
	}
	wynik = wynik / rozmiar;
	std::cout << wynik << std::endl;
}

int main() {

	int rozmiar;
	std::cout << "Podaj rozmiar zbioru liczb" << std::endl;
	std::cin >> rozmiar;

	int* tablica = new int[rozmiar];

	sredniaArytmetyczna(rozmiar, tablica);

	delete[] tablica;

	return 0;
}
