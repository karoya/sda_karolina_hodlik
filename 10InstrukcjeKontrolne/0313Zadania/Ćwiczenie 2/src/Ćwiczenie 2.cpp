#include <iostream>

const int MAKSYMALNY_ROZMIAR_STOSU = 5;

struct Stos
{
	int wierzcholek; // pozycja w tablicy "dane" - gdzie zapisa� nowy element
	int *dane;
};

void pokazRozmiar(Stos stosLiczb)
{
	// poka� rozmiar stosu
	std::cout << "Rozmiar: " << stosLiczb.wierzcholek << std::endl;
}

void pokazWierzcholek(Stos stosLiczb)
{
	// sprawd�, co jest na wierzchu stosu
	// je�li stos jest pusty, to poinformuj o tym
	if (stosLiczb.wierzcholek == 0)
	{
		std::cout << "Stos jest pusty!" << std::endl;
	}
	else
	{
		// w przeciwnym wypadku - poka� wierzcho�ek
		// ostatni wstawiony element znajduje si� o 1 ni�ej ni� miejsce na nowy element
		int wierzch = stosLiczb.dane[stosLiczb.wierzcholek - 1];
		std::cout << "Na g�rze jest: " << wierzch << std::endl;
	}
}

void wstaw(Stos& stosLiczb, int nowaWartosc)
{
	if (stosLiczb.wierzcholek >= rozmiar)
	{
		std::cerr << "Stos pe�ny!" << std::endl;
		return;
	}

	// wstawi� liczb� na szczyt stosu
	stosLiczb.dane[stosLiczb.wierzcholek] = nowaWartosc; // zapisz dane na wierzcho�ku stosu
	stosLiczb.wierzcholek += 1;			 				// przesuwam wierzcho�ek
}

int zdejmij(Stos& stosLiczb)
{
	if (stosLiczb.wierzcholek == 0)
	{
		std::cerr << "Pr�ba zdj�cia z pustego stosu!" << std::endl;
		return 0;
	}

	stosLiczb.wierzcholek -= 1; // przesuwam wierzcho�ek na pierwszy istniej�cy element
	int zdjete = stosLiczb.dane[stosLiczb.wierzcholek]; // odczytaj dane z wierzchu stosu
	return zdjete;
}

int main()
{
	std::cout << "Poka� rozmiar stosu" << std::endl;
	int rozmiar;
	std::cin >> rozmiar;

	if (rozmiar <=0){
		std::cerr <<  "Stos musi miec wiekszy rozmiar" << std::endl;
		return 1;
	}
	// nowy stos
	Stos liczby;

	// zerowanie stosu
	liczby.wierzcholek = 0;
	for (int i = 0; i < MAKSYMALNY_ROZMIAR_STOSU; ++i)
	{
		liczby.dane[i] = 0;
	}

	pokazRozmiar(liczby);

	wstaw(liczby, 4);

	pokazWierzcholek(liczby);

	pokazRozmiar(liczby);

	wstaw(liczby, 7);

	pokazWierzcholek(liczby);

	pokazRozmiar(liczby);

	int zdjete1 = zdejmij(liczby);
	std::cout << "Zdj�te: " << zdjete1 << std::endl;

	pokazWierzcholek(liczby);

	pokazRozmiar(liczby);

	int zdjete2 = zdejmij(liczby);
	std::cout << "Zdj�te: " << zdjete2 << std::endl;

	pokazWierzcholek(liczby);

	pokazRozmiar(liczby);

	int zdjete3 = zdejmij(liczby);
	std::cout << "Zdj�te: " << zdjete3 << std::endl;

	wstaw(liczby, 42);
	wstaw(liczby, 128);
	wstaw(liczby, -1);
	wstaw(liczby, 8);
	wstaw(liczby, -4);
	wstaw(liczby, 10);
	wstaw(liczby, 100);

	pokazRozmiar(liczby);
	pokazWierzcholek(liczby);

	wstaw(liczby, 100);

	return 0;
}
