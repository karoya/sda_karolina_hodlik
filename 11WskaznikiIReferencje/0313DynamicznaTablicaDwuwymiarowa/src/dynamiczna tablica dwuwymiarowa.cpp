#include <iostream>

void pokaz(float** m, int maxX, int maxY);

int main()
{
	const int stala[5][5] = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12,
			13, 14, 15 }, { 16, 17, 18, 19, 20 } };

	std::cout << stala[1][4] << std::endl;

	float** macierz = NULL;

	macierz = new float*[4];
	for (int i = 0; i < 4; ++i)
	{
		macierz[i] = new float[5];
	}
	for (int z = 0; z < 4; ++z)
	{
			for (int i = 0; i < 5; ++i)
	{
		macierz[z][i] = 1.5f * stala[z][i];

	}
	}

	pokaz(macierz, 5, 4);

	for (int z = 0; z < 4; ++z)
	{
		delete[] macierz[z];
	}
	delete[] macierz;

	return 0;
}

void pokaz(float** m, int maxX, int maxY)
{
	for (int y = 0; y < maxY; ++y)
	{
		for (int x = 0; x < maxX; ++x)
		{
			std::cout << m[y][x] << ' ';
		}
		std::cout << std::endl;
	}

}
