/*
 * CezarTekst.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "CezarTekst.hpp"
#include <iostream>
#include <string>

CezarTekst::CezarTekst()

{
    std::cout << "CezarTekst" <<std::endl;
}

CezarTekst::~CezarTekst()

{
    std::cout << "~CezarTekst" <<std::endl;
}
void CezarTekst::szyfruj()

{
    std::string zmieniony;
    for (unsigned int i = 0; i < getTekst().length(); i++)
    {
    	zmieniony += 'a' + (getTekst()[i] - 'a' + 13) % 26;
    }
    std::cout << "Zaszyfrowany tekst Cezar " << zmieniony <<std::endl;
    setTekst(zmieniony);
}

void CezarTekst::wypisz()
{
	std::cout << "Cezar tekst: " << getTekst() << std::endl;
}



