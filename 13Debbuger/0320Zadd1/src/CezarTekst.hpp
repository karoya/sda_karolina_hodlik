/*
 * CezarTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */
#ifndef CezarTekst_hpp
#define CezarTekst_hpp
#include "SzyfrowanyTekst.hpp"
#include <string>
class CezarTekst: public SzyfrowanyTekst

{
private:
public:
    CezarTekst();
    ~CezarTekst();
    void szyfruj();
    void wypisz();
};

#endif /* CezarTekst_hpp */
