/*
 * PodstawieniowyTekst.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "PodstawieniowyTekst.hpp"
#include <iostream>

PodstawieniowyTekst::PodstawieniowyTekst() {

	std::cout << "PodstawieniowyTekst" <<std::endl;
}

PodstawieniowyTekst::~PodstawieniowyTekst() {

	std::cout << "~PodstawieniowyTekst" <<std::endl;
}
void PodstawieniowyTekst::szyfruj()

{
    std::string zmieniony;

    for (unsigned int i =0; i < getTekst().length(); i++)
    {
    	int poz =  getTekst()[i] - 'a';
    	zmieniony += szyfr[poz];
    }
    std::cout << "Zaszyfrowany tekst Podstawieniowy " << zmieniony <<std::endl;
    setTekst(zmieniony);
}

void PodstawieniowyTekst::wypisz()
{
	std::cout << "PodstawieniowyTekst: " << getTekst() << std::endl;
}
