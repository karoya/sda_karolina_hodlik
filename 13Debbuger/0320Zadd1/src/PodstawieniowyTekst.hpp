/*
 * PodstawieniowyTekst.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef PODSTAWIENIOWYTEKST_HPP_
#define PODSTAWIENIOWYTEKST_HPP_
#include "SzyfrowanyTekst.hpp"

class PodstawieniowyTekst: public SzyfrowanyTekst {
protected:
	const std::string szyfr = "BBBCBBBCBBBCBBBCBBBCBBBCBB";
public:
	PodstawieniowyTekst();
	~PodstawieniowyTekst();
	void szyfruj();
	void wypisz();
};

#endif /* PODSTAWIENIOWYTEKST_HPP_ */
