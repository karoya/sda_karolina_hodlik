/*
 * SzyfrowanyTekst.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "SzyfrowanyTekst.hpp"

#include <iostream>

SzyfrowanyTekst::SzyfrowanyTekst()
:mZaszyfrowany(false){

	std::cout << "SzyfrowanyTekst" << std::endl;

}

bool SzyfrowanyTekst::isZaszyfrowany() const {
	return mZaszyfrowany;
}

void SzyfrowanyTekst::setZaszyfrowany(bool zaszyfrowany) {
	mZaszyfrowany = zaszyfrowany;
}

SzyfrowanyTekst::~SzyfrowanyTekst()
{
	std::cout << "~SzyfrowanyTekst" << std::endl;
}

