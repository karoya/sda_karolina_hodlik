/*
 * SzyfrowanyTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */
#include "Wypisywalny.hpp"
#ifndef SZYFROWANYTEKST_HPP_
#define SZYFROWANYTEKST_HPP_

class SzyfrowanyTekst: public Wypisywalny {
protected:
	bool mZaszyfrowany;
public:
	SzyfrowanyTekst();
	virtual ~SzyfrowanyTekst();
	virtual void szyfruj() = 0;
	virtual void wypisz() = 0;
	bool isZaszyfrowany() const;
	void setZaszyfrowany(bool zaszyfrowany = true);
};

#endif /* SZYFROWANYTEKST_HPP_ */
