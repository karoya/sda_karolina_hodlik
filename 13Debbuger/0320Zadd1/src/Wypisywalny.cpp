/*
 * Wypisywalny.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include "Wypisywalny.hpp"
#include <iostream>

Wypisywalny::Wypisywalny()
:mTekst("")
{

}

Wypisywalny::Wypisywalny(std::string tekst)
:mTekst(tekst)
{
	setTekst(tekst);
	std::cout << "Tworz�: " << getTekst() << std::endl;

}

const std::string& Wypisywalny::getTekst() const
{
	return mTekst;
}

void Wypisywalny::setTekst(std::string tekst)
{
	for (unsigned int i = 0; i < tekst.length(); i++)
	{
		tekst[i] = tolower(tekst[i]);
	}
	mTekst = tekst;
}

Wypisywalny::~Wypisywalny()
{
	std::cout << "Niszcz�:" << getTekst() <<  std::endl;
}

void Wypisywalny::wypisz()
{
	std::cout <<"Wyisywalne: "<< mTekst << std::endl;
}
