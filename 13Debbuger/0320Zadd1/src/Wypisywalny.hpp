/*
 * Wypisywalny.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef WYPISYWALNY_HPP_
#define WYPISYWALNY_HPP_
#include <string>

class Wypisywalny {
private:
	std::string mTekst;
public:

	Wypisywalny();
	Wypisywalny(std::string tekst);
	~Wypisywalny();
	const std::string& getTekst() const;
	void setTekst(std::string tekst);
	virtual void wypisz();
};

#endif /* WYPISYWALNY_HPP_ */
