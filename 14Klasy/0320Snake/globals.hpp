#ifndef _GLOBALS_HPP_
#define _GLOBALS_HPP_

enum state_enum { PAUSE, CRASH, QUIT, RUNNING };

enum direction_enum { STOP, UP, DOWN, LEFT, RIGHT };

struct state {
  enum state_enum game_state;
  enum direction_enum direction;
};

#define BOARD_LENGTH 70
#define BOARD_WIDTH 30
#define BOARD_BORDER_THICKNESS 1
#define BOARD_BORDER_SYMBOL '*'
#define FRUIT_SYMBOL '@'
#define SNAKE_SYMBOL 'O'
#define GAME_SPEED 100000000 /* nanoseconds */

#define SCORE_FRUIT 10

#define BOARD_LENGTH_MIN 10
#define BOARD_LENGTH_MAX 80
#define BOARD_WIDTH_MIN 10
#define BOARD_WIDTH_MAX 40
#define BOARD_THICKNESS_MIN 1
#define BOARD_THICKNESS_MAX 5
#define BOARD_BORDER_SYMBOL_MIN 32
#define BOARD_BORDER_SYMBOL_MAX 126
#define SNAKE_SYMBOL_MIN 32
#define SNAKE_SYMBOL_MAX 126
#define FRUIT_SYMBOL_MIN 32
#define FRUIT_SYMBOL_MAX 126

#define GAME_PAUSE 'p'
#define GAME_QUIT 'q'

#define MOVE_UP 'w'
#define MOVE_DOWN 's'
#define MOVE_LEFT 'a'
#define MOVE_RIGHT 'd'

#endif /* _GLOBALS_HPP_ */
