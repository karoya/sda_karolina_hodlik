#include <iostream>

#include "input.hpp"

char read_key() {
  char ch = '\0';
  std::cin>>ch;
  return ch;
}
