//============================================================================
// Name        : Klasy.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <iostream>
class klasa
{
protected:
	int i;

public:
	klasa()
{
		i = 1;
		std::cout << "konstruktor " << i << std::endl;
}
	klasa(const klasa& z)
	{
		i = z.i + 1;
		std::cout <<"konstruktor kop " << i << std::endl;
	}

	~klasa()
	{
		std::cout << "destruktor " << i << std::endl;
	}
};
void funkcja(klasa a)
	{
		klasa b(a);
	}

//class pochodna: public klasa
//{
//public:
//
//	pochodna():
//	{
//		std::cout << "Hello1" << std::endl;
//	}
//	~pochodna()
//	{
//		std::cout << "Bye1" << std::endl;
//	}
//};

int main() {
	klasa a;
//	klasa b(a);
	funkcja(a);
//	klasa* b = new klasa();
//	pochodna i;

//	delete b;


	return 0;
}
