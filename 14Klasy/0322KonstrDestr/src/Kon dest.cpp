#include <iostream>

class A {
public:
	A(int x) {
		std::cout << "Konstruktor A" << std::endl;
	}
	~A() {
		std::cout << "Destruktor A" << std::endl;
	}
};

class B: public A {
public:
	B(int y, int z): A(y)
{
		std::cout << "Konstruktor B" << std::endl;
	}
	~B() {
		std::cout << "Destruktor B" << std::endl;
	}
};
class C
{
public:
	C() {
		std::cout << "Konstruktor C" << std::endl;
	}
	~C() {
		std::cout << "Destruktor C" << std::endl;
	}
};
class D: public C, public B
{
public:
	D() {
		std::cout << "Konstruktor D" << std::endl;
	}
	~D() {
		std::cout << "Destruktor D" << std::endl;
	}
};

void separator() {
	std::cout << "-----------------" << std::endl;
}

int main() {
	separator();
	A a(y);
	separator();
	B b;
	separator();
	C c;
	separator();
	D d;
	separator();

	return 0;

}
