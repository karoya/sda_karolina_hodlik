//============================================================================
// Name        : PoczatekSrodekKoniecKlasy.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

class Poczatek {
protected:
	int mX;
public:
	Poczatek() :
			mX(0) {
		std::cout << "K - Poczatek" << std::endl;
	}
	Poczatek(int x) :
			mX(x) {
		std::cout << "Nowe mX: " << mX << std::endl;
	}
	~Poczatek() {
		std::cout << "D - Poczatek" << std::endl;
	}
	void przedstawSie() {
		std::cout << "Poczatek" << std::endl;
	}
	void zmienX() {
		mX += 1;
	}
	void wypiszX() {
		std::cout << "X: " << mX << std::endl;
	}

};
class Srodek: public Poczatek {
protected:
	int mX;
public:
	Srodek() :
			mX(10) {
		std::cout << "K - Srodek" << std::endl;
	}
	Srodek(int x) :
			mX(x) {
		std::cout << "Nowe mX: " << mX << std::endl;
	}
	~Srodek() {
		std::cout << "D - Srodek" << std::endl;
	}

	void przedstawSie() {
		std::cout << "Srodek" << std::endl;
	}

	void zmienX() {
		Poczatek::mX += 1;
		mX += 1;
	}
	void wypiszX() {
		std::cout << "X: " << mX << std::endl;
		std::cout << "Poczatek X: " << Poczatek::mX << std::endl;
	}

};
class Koniec: public Srodek {
protected:
	int mX;
public:
	Koniec() :
			mX(100) {
		std::cout << "K - Koniec" << std::endl;
	}
	Koniec(int x) :
			mX(x) {
		std::cout << "Nowe mX: " << mX << std::endl;
	}
	~Koniec() {
		std::cout << "D - Koniec" << std::endl;
	}
	void przedstawSie() {
		std::cout << "Koniec" << std::endl;
	}
	void zmienX() {
		Srodek::Poczatek::mX += 1;
		Srodek::mX += 1;
		mX += 1;
	}
	void wypiszX() {
		std::cout << "X: " << mX << std::endl;
		std::cout << "Srodek X: " << Srodek::mX << std::endl;
		std::cout << "Poczatek X: " << Srodek::Poczatek::mX << std::endl;
	}
};

int main() {

	Poczatek poczatek;
	poczatek.przedstawSie();
	poczatek.zmienX();
	poczatek.wypiszX();
	Srodek srodek;
	srodek.przedstawSie();
	srodek.zmienX();
	srodek.wypiszX();
	Koniec koniec;
	koniec.przedstawSie();
	koniec.zmienX();
	koniec.wypiszX();

	Poczatek poczatek1(22);
	Srodek srodek1(23);
	Koniec koniec1(24);

	return 0;
}
