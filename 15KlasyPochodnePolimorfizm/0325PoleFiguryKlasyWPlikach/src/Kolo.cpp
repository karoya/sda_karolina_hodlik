/*
 * Kolo.cpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#include "Kolo.h"

Kolo::Kolo(float r): mR(r), mPi(3.14) {
}

float Kolo::pole(float r)
{
	return mPi*mR*mR;
}
