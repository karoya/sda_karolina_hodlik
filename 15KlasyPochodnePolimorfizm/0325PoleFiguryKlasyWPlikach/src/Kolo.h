/*
 * Kolo.h
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef KOLO_H_
#define KOLO_H_
#include "Figura.h"

class Kolo: public Figura {
	float mR;
	const float mPi;
public:
	Kolo(float r);
	float pole(float r);

};

#endif /* KOLO_H_ */
