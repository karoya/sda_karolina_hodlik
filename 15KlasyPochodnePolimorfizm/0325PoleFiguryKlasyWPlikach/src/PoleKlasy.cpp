#include <iostream>
 class Figura
 {
   public:
     virtual float pole() const
     {
       return -1.0;
     }
 };

 class Prostokat : public Figura
 {
   public:
	 Prostokat( const float a, const float b )
   	   : mA( a )
   	   , mB(b) {}

     float pole() const
     {
       return mA * mB;
     }
   private:
     float mA;
     float mB;
 };

 class Kolo : public Figura
 {
   public:
     Kolo( const float promien ) : mR( promien ) {}

     float pole() const
     {
       return pi * mR * mR;
     }
   private:
     float mR; // promien kola
     const float pi = 3.14159;
 };

 void wyswietlPole( Figura &figura )
 {
   std::cout << figura.pole() << std::endl;
   return;
 }

 int main()
 {
   // deklaracje obiektow:
   Figura jakasFigura;
   Prostokat jakisProstokat( 5 , 10);
   Kolo jakiesKolo( 3 );

   Figura *wskJakasFigura = 0; // deklaracja wska�nika

   // obiekty -------------------------------
   std::cout << jakasFigura.pole() << std::endl;
   std::cout << jakisProstokat.pole() << std::endl;
   std::cout << jakiesKolo.pole() << std::endl;

   // wskazniki -----------------------------
   wskJakasFigura = &jakasFigura;
   std::cout << wskJakasFigura->pole() << std::endl;
   wskJakasFigura = &jakisProstokat;
   std::cout << wskJakasFigura->pole() << std::endl;
   wskJakasFigura = &jakiesKolo;
   std::cout << wskJakasFigura->pole() << std::endl;

   // referencje -----------------------------
   wyswietlPole( jakasFigura );
   wyswietlPole( jakisProstokat );
   wyswietlPole( jakiesKolo );

   return 0;
 }

