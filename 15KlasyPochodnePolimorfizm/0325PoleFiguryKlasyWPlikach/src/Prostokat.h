/*
 * Prostokat.h
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef PROSTOKAT_H_
#define PROSTOKAT_H_
#include "Figura.h"

class Prostokat: public Figura {
	float mA;
	float mB;
public:
	Prostokat(float a, float b);
	float pole(float a, float b);
};

#endif /* PROSTOKAT_H_ */
