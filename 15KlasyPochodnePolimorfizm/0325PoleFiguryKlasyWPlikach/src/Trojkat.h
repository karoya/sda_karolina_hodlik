/*
 * Trojkat.h
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef TROJKAT_H_
#define TROJKAT_H_
#include "Figura.h"

class Trojkat: public Figura {
	float mH;
	const float mA;
public:
	Trojkat(float a, float h);
	float pole(float a, float h);
};

#endif /* TROJKAT_H_ */
