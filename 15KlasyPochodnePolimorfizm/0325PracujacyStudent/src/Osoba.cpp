/*
 * Osoba.cpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#include "Osoba.h"
#include <iostream>

Osoba::Osoba(long pesel)
: mPesel(pesel)
{

}

long Osoba::podajPesel()
{
	std::cout << "Pesel " << mPesel << std::endl;
	return mPesel;
}
