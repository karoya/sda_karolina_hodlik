/*
 * Pracownik.h
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include <string>
#include <iostream>
#include "Osoba.h"

#ifndef PRACOWNIK_H_
#define PRACOWNIK_H_

class Pracownik: public virtual Osoba {
	float mPensja;
	std::string mZawod;
public:
	Pracownik(float pensja, std::string zawod, long pesel);
	Pracownik(float pensja, std::string zawod);
	void pracuj();
	virtual void pracuj1();
};

#endif /* PRACOWNIK_H_ */
