/*
 * PracujacyStudent.h
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include <string>
#include "Pracownik.h"
#include "Student.h"

#ifndef PRACUJACYSTUDENT_H_
#define PRACUJACYSTUDENT_H_

class PracujacyStudent: public Student, public Pracownik {
public:
	PracujacyStudent(int nrIndeksu, std::string kierunek, float pensja, std::string zawod);
	PracujacyStudent(int nrIndeksu, std::string kierunek, float pensja, std::string zawod, long pesel);
	void pracuj();
	void uczSie();
};

#endif /* PRACUJACYSTUDENT_H_ */
