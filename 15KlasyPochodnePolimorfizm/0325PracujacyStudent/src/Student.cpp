/*
 * Student.cpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include <iostream>
#include "Student.h"
#include "Osoba.h"
Student::Student(int nrIndeksu, std::string kierunek, long pesel):
Osoba(pesel),
mNrIndeksu(nrIndeksu), mKierunek(kierunek)
{

};
Student::Student(int nrIndeksu, std::string kierunek):
mNrIndeksu(nrIndeksu), mKierunek(kierunek)
{

};

int Student::podajNumer(){
	return mNrIndeksu;
}
void Student::uczSie(){
	std::cout << "Ucz sie!" << std::endl;
}
