/*
 * Student.h
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include <iostream>
#include <string>
#include "Osoba.h"

#ifndef STUDENT_H_
#define STUDENT_H_

class Student: public virtual Osoba {
	int mNrIndeksu;
	std::string mKierunek;
public:
	Student(int nrIndeksu, std::string kierunek, long pesel);
	Student(int nrIndeksu, std::string kierunek);

	int podajNumer();
	void virtual uczSie();
};

#endif /* STUDENT_H_ */
