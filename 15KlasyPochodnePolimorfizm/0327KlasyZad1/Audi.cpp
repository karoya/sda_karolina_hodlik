/*
 * Audi.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Audi.hpp"

Audi::Audi(std::string kolor, std::string marka, float pojemnosc)
: Samochod(kolor, marka, pojemnosc)
{
	std::cout<< "Audi" << std::endl;
}

Audi::~Audi() {

	std::cout<< "~Audi" << std::endl;
}

void Audi::kolor1() const
{
	std::cout << "Kolor Audi: " << mKolor << std::endl;
}
void Audi::marka1() const
{
	std::cout << "Marka Audi: "<< mMarka <<std::endl;
}
void Audi::pojemnosc1() const
{
	std::cout<< "Pojemnosc Audi: " << mPojemnosc <<std::endl;
}
