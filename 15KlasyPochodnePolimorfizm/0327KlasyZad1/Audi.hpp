/*
 * Audi.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */
#include <string>
#include<iostream>
#include "Samochod.h"
#ifndef AUDI_HPP_
#define AUDI_HPP_

class Audi: public Samochod {
public:
	Audi(std::string kolor, std::string marka, float pojemnosc);
	~Audi();
	void kolor1() const;
	void marka1() const;
	void pojemnosc1() const;

};

#endif /* AUDI_HPP_ */
