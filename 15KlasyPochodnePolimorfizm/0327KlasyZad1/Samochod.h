/*
 * Samochod.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */
#include <string>
#include <iostream>
#ifndef SAMOCHOD_H_
#define SAMOCHOD_H_

class Samochod {

protected:
	std::string mKolor;
	std::string mMarka;
	float mPojemnosc;

public:
	Samochod(std::string kolor, std::string marka, float pojemnosc)
	: mKolor(kolor),
	  mMarka(marka),
	  mPojemnosc(pojemnosc)
	  {
		std::cout << "Samochod" << std::endl;
	  };
	virtual ~Samochod()
	{
		std::cout << "~Samochod" << std::endl;
	};
	virtual void kolor1() const = 0;
	virtual void marka1() const = 0;
	virtual void pojemnosc1() const = 0;
};

#endif /* SAMOCHOD_H_ */
