/*
 * VW.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "VW.hpp"
#include<string>
#include <iostream>
VW::VW(std::string kolor, std::string marka, float pojemnosc)
: Samochod(kolor, marka, pojemnosc)
{
	std::cout<< "VW" << std::endl;
}
VW::~VW()
{
	std::cout<< "~VW" << std::endl;
}
void VW::kolor1() const
{
	std::cout << "Kolor VW: " << mKolor << std::endl;
}
void VW::marka1() const
{
	std::cout << "Marka VW: "  << mMarka <<std::endl;
}
void VW::pojemnosc1() const
{
	std::cout << "Pojemnosc VW: " << mPojemnosc <<std::endl;
}
