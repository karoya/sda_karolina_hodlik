/*
 * VW.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */
#include "Samochod.h"
#include <string>

#ifndef VW_HPP_
#define VW_HPP_

class VW: public Samochod {
public:
	VW(std::string kolor, std::string marka, float pojemnosc);
	~VW();
void kolor1() const;
void marka1() const;
void pojemnosc1() const;
};

#endif /* VW_HPP_ */
