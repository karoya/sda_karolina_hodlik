#include <iostream>
#include "Audi.hpp"
#include "VW.hpp"

int main(){

	Samochod* tablica[3];
	tablica[0] = new Audi("zielony", "Audi", 1.6f);
	tablica[1] = new VW("czerwony", "VW", 2.0f);
	tablica[2] =  new Audi("niebieski", "Audi", 1.8f);

	for (int i = 0; i < 3; i++)
	{
		tablica[i] -> kolor1();
		tablica[i] -> marka1();
		tablica[i] -> pojemnosc1();
	}

	for (int i = 0; i < 3; i++)
		{
			delete tablica[i];
		}

	return 0;
}
