/*
 * Lista.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include <iostream>
#include <string>
using namespace std;

class ListaStringow {
private:
	class Node {
	private:
		string mDane;
		Node* mNastepny;
	public:
		string getDane() {
			return mDane;
		}

		void setDane(string dane) {
			mDane = dane;
		}

		Node* getNastepny() {
			return mNastepny;
		}

		void setNastepny(Node* nastepny) {
			mNastepny = nastepny;
		}

		Node(string dane, Node* nastepny) :
				mDane(dane), mNastepny(nastepny) {
		};
	};

	Node* mPierwszy;
	int mRozmiar;
public:
	void dodaj(string lancuch);
	bool czyPusta();
	void wypisz();

	ListaStringow();
};

