/*
 * KolorowyPunkt.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#include "KolorowyPunkt.hpp"

KolorowyPunkt::KolorowyPunkt(Color kolor, int x, int y)
:Punkt(x, y),
 mKolor(kolor)
 {

}


void KolorowyPunkt::wypisz(){

	std::cout << "x= " << getX() << std::endl;
	std::cout << "y= " << getY() << std::endl;
	switch(mKolor)
	{
	case czerwony:
		std::cout << "Czerwony\n";
		break;
	case zielony:
		std::cout << "Zielony\n";
		break;
	case niebieski:
		std::cout << "Niebieski\n";
		break;
	case pomaranczowy:
		std::cout << "Pomaranczowy\n";
		break;
	default:
		std::cout << "�mieci\n";
	}}
