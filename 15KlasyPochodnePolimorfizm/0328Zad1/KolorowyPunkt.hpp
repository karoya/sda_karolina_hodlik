/*
 * KolorowyPunkt.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef KOLOROWYPUNKT_HPP_
#define KOLOROWYPUNKT_HPP_
#include "Punkt.hpp"
#include <iostream>

class KolorowyPunkt: public Punkt {

public:
	enum Color
	{
		czerwony,
		zielony,
		niebieski,
		pomaranczowy
	};
	KolorowyPunkt(Color kolor, int x, int y);
	void wypisz();

protected:
	Color mKolor;

};

#endif /* KOLOROWYPUNKT_HPP_ */
