/*
 * Punkt.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#include "Punkt.hpp"
#include <cmath>

Punkt::Punkt()
:mX(0),
 mY(0)
{
}
Punkt::Punkt(int x, int y) :
		mX(x), mY(y) {
	if (mX > 800) {
			mX = 800;
		} else {
			mX = x;
		}
	if (mY> 800) {
			mY = 800;
		} else {
			mY = y;
		}
}

int Punkt::getX() const {
	return mX;
}
void Punkt::setX(int x) {
	if (mX + x > 800) {
		mX = 800;
	} else {
		mX += x;
	}
}
int Punkt::getY() const {
	return mY;
}
void Punkt::setY(int y) {
	if (mY + y > 800) {
		mY = 800;
	} else {
		mY += y;
	}
}
void Punkt::wypisz() {
	std::cout << "x= " << mX << std::endl;
	std::cout << "y= " << mY << std::endl;
}

void Punkt::przesun(int x, int y) {
	przesunX(x);
	przesunY(y);

}
void Punkt::przesun(Punkt p) {
	przesun(p.getX(), p.getY()); // mX += p.getX(), mY += p.getY()
}
void Punkt::przesunX(int x) {
	setX(mX+x);
}
void Punkt::przesunY(int y) {
	setY(mY+y);
}
void Punkt::obliczOdleglosc(int x, int y) {

	float a,b;
	float wynik = 0;
	a=getX()-x;
	b=getY()-y;
	wynik = sqrt(a*a+b*b);
	std::cout << "Odleglosc: " << wynik << std::endl;
}

void Punkt::obliczOdleglosc(Punkt p) {
	obliczOdleglosc(p.getX(), p.getY());
}
