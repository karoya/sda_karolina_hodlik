/*
 * Punkt.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */
#include <iostream>
#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt {
private:
	int mX;
	int mY;
public:
	Punkt();
	Punkt(int x, int y);

	int getX() const;
	void setX(int x);
	int getY() const;
	void setY(int y);
	void wypisz();
	void przesun(int x, int y);
	void przesun(Punkt p);
	void przesunX(int x);
	void przesunY(int y);
	void obliczOdleglosc(int x, int y);
	void obliczOdleglosc(Punkt p);

};

#endif /* PUNKT_HPP_ */
