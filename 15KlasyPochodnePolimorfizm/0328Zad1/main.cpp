/*
 * main.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */
#include "Punkt.hpp"
#include "KolorowyPunkt.hpp"


int main()
{
	KolorowyPunkt p1(KolorowyPunkt::czerwony, 1000, 4);
	Punkt p2(1000,5);

	p1.wypisz();
	p2.wypisz();

	p1.obliczOdleglosc(p2);

	p1.przesun(p2);
	p1.wypisz();
	p2.wypisz();

	p1.przesunY(4);
	p1.wypisz();

	p1.obliczOdleglosc(p2);
	p1.wypisz();
	p2.wypisz();
	return 0;
}


