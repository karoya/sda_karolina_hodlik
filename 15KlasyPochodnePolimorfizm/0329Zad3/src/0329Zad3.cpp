//============================================================================
// Name        : 0329Zad3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Czas.hpp"
using namespace std;

int main() {

	Czas czas1(4, 40, 70);
	Czas czas2(4, 20, 70);

	czas1.wypisz();
	czas2.wypisz();

	czas1.obliczRoznice(czas2);

	czas1.przesunSekundy(3);
	czas1.wypisz();
	czas2.wypisz();

	czas1.obliczRoznice(czas2);

	return 0;
}
