/*
 * Czas.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef CZAS_HPP_
#define CZAS_HPP_

class Czas {
private:
	unsigned int mGodziny;
	unsigned int mMinuty;
	unsigned int mSekundy;

public:

	Czas(unsigned int godziny,unsigned int minuty,unsigned int sekundy);

	unsigned int getGodziny() const;
	void setGodziny(unsigned int godziny);
	unsigned int getMinuty() const;
	void setMinuty(unsigned int minuty);
	unsigned int getSekundy() const;
	void setSekundy(unsigned int sekundy);
	void wypisz();
	void przesunGodziny(unsigned int godziny);
	void przesunMinuty(unsigned int minuty);
	void przesunSekundy(unsigned int sekundy);
	void obliczRoznice(Czas czas);

};

#endif /* CZAS_HPP_ */
