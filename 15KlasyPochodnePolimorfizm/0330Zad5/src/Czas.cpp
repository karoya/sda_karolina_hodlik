/*
 * Czas.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "Czas.hpp"
#include<iostream>

Czas::Czas(unsigned int godziny,unsigned int minuty,unsigned int sekundy):
mGodziny(godziny),
mMinuty(minuty),
mSekundy(sekundy)
{
if (mGodziny > 24)
{
	mGodziny = 24;
}
else
	{
		mGodziny = godziny;
	}
if (mMinuty > 60)
{
	mMinuty = 60;
}
else
	mMinuty = minuty;
if (mSekundy > 60)
{
	mSekundy = 60;
}
else
	mSekundy = sekundy;
}


unsigned int Czas::getGodziny() const {
	return mGodziny;
}

void Czas::setGodziny(unsigned int godziny) {
	mGodziny = godziny;
}

unsigned int Czas::getMinuty() const {
	return mMinuty;
}

void Czas::setMinuty(unsigned int minuty) {
	mMinuty = minuty;
}

unsigned int Czas::getSekundy() const {
	return mSekundy;
}

void Czas::setSekundy(unsigned int sekundy) {
	mSekundy = sekundy;
}

void Czas::wypisz()
{
	std::cout << "Godzin: " << mGodziny << std::flush;
	std::cout << " Minut: " << mMinuty << std::flush;
	std::cout << " Sekund: " << mSekundy << std::endl;

}

void Czas::przesunGodziny(unsigned int godziny)
{
	mGodziny += godziny;
}
void Czas::przesunMinuty(unsigned int minuty)
{
	mMinuty += minuty;
}
void Czas::przesunSekundy(unsigned int sekundy)
{
	mSekundy += sekundy;
}

void Czas::obliczRoznice(Czas czas)
{
	unsigned int iloscSekundPierwszyCzas = (getGodziny() * 3600) + (getMinuty() * 60) + getSekundy();
	unsigned int iloscSekundDrugiCzas = (czas.getGodziny() * 3600) + (czas.getMinuty() * 60) + czas.getSekundy();

	int roznica = iloscSekundPierwszyCzas - iloscSekundDrugiCzas;

	int ileGodz = roznica / 3600;
	int ileSek = roznica % 3600;
	int ileMin = 0;
	if (ileSek >= 60)
	{
		ileMin = ileSek / 60;
		ileSek = ileSek % 60;
	}

	std::cout << "Roznica to: godzin - " << ileGodz << ", min - " << ileMin << ", sek - " << ileSek << std::endl;

}
