/*
 * Data.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Data.hpp"
#include <iostream>

Data::Data()
: mDzien(0),
  mMiesiac(0),
  mRok(0)
{
};

Data::Data(unsigned int dzien, unsigned int miesiac, unsigned int rok)
: mDzien(dzien),
  mMiesiac(miesiac),
  mRok(rok)
{

};

unsigned int Data::getDzien() const {
	return mDzien;
};

void Data::setDzien(unsigned int dzien) {
	if (dzien < 1) {
		mDzien = 1;
	} else if (dzien > 31) {
		mDzien = 31;
	} else {
		mDzien = dzien;
	}
};

unsigned int Data::getMiesiac() const {
	return mMiesiac;
};

void Data::setMiesiac(unsigned int miesiac) {
	if (miesiac < 1) {
		mMiesiac = 1;
	} else if (miesiac > 12) {
		mMiesiac = 12;
	} else {
		mMiesiac = miesiac;
	}
};

unsigned int Data::getRok() const {
	return mRok;
};

void Data::setRok(unsigned int rok) {
	if (rok < 1900) {
		mRok = 1900;
	} else if (rok > 2017) {
		mRok = 2017;
	} else {
		mRok = rok;
	}
}
void Data::wypisz()
{
	std::cout << "Dzien: " << mDzien << std::endl;
	std::cout << "Miesiac: " << mMiesiac << std::endl;
	std::cout << "Rok: " << mRok << std::endl;
}
void Data::przesunDzien(unsigned int dzien)
{
	mDzien += dzien;
}
void Data::przesunMiesiac(unsigned int miesiac)
{
	mMiesiac += miesiac;
}
void Data::przesunRok(unsigned int rok)
{
	mRok += rok;
}

void Data::obliczRoznice(Data data)
{

	int iloscDniPierwszaData = (getRok() * 365) + getDzien();
	iloscDniPierwszaData += (getMiesiac() - 1) * 31;

	int iloscDniDrugaData = (data.getRok() * 365) + data.getDzien();
	iloscDniDrugaData += (data.getMiesiac() - 1) * 31;

	int roznica = iloscDniPierwszaData - iloscDniDrugaData;
	int ileLat = roznica / 365;
	int ileDni = roznica % 365;
	int ileMsc = 0;
	if (ileDni > 31)
	{
		ileMsc = ileDni /31;
		ileDni = ileDni % ileMsc;
	}

	std::cout << "Roznica to: dni - " << ileDni << " miesiecy - " << ileMsc << " lat - " << ileLat << std::endl;

}

