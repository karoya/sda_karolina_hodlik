/*
 * Data.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef DATA_HPP_
#define DATA_HPP_
#include<string>

class Data {
private:
	unsigned int mDzien;
	unsigned int mMiesiac;
	unsigned int mRok;
public:
	Data();
	Data(unsigned int dzien, unsigned int miesiac, unsigned int rok);

	unsigned int getDzien() const;
	void setDzien(unsigned int dzien);
	unsigned int getMiesiac() const;
	void setMiesiac(unsigned int miesiac);
	unsigned int getRok() const;
	void setRok(unsigned int rok);
	void wypisz();
	void przesunDzien(unsigned int dzien);
	void przesunMiesiac(unsigned int miesiac);
	void przesunRok(unsigned int rok);
	void obliczRoznice(Data data);



};

#endif /* DATA_HPP_ */
