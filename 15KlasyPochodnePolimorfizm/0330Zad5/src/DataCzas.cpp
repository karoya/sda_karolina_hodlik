/*
 * DataCzas.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "DataCzas.hpp"
#include <iostream>

DataCzas::DataCzas()
:Data(0, 0, 0),
 Czas(0, 0, 0)
{

}
DataCzas::DataCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok, unsigned int godziny,unsigned int minuty,unsigned int sekundy)
:Data(dzien, miesiac, rok),
 Czas(godziny, minuty, sekundy)
{

}
DataCzas::DataCzas(Data data, Czas czas)
: Data(data.getDzien(), data.getMiesiac(), data.getRok()),
  Czas(czas.getGodziny(), czas.getMinuty(),czas.getSekundy())
{

}

void DataCzas::setData(unsigned int dzien, unsigned int miesiac, unsigned int rok)
{
	setDzien(dzien);
	setMiesiac(miesiac);
	setRok(rok);

}
void DataCzas::setCzas(unsigned int godziny,unsigned int minuty,unsigned int sekundy)
{
	setGodziny(godziny);
	setMinuty(minuty);
	setSekundy(sekundy);

}
Czas DataCzas::getCzas() const
{
	return Czas(getGodziny(), getMinuty(), getSekundy());

}
Data DataCzas::getData() const
{
	return Data(getDzien(), getMiesiac(), getRok());
}

void DataCzas::wypisz()

{

    std::cout << "Dzien: " << getDzien() << std::flush;

    std::cout << ", Miesiac: " << getMiesiac() << std::flush;

    std::cout << ", Rok: " << getRok() << std::endl;

    std::cout << "Godzina: " << getGodziny() << std::flush;

    std::cout << ", Minut: " << getMinuty() << std::flush;

    std::cout << ", Sekundy: " << getSekundy() << std::endl;

}

void DataCzas::przesunDataCzas(DataCzas dataczas)

{

	przesunGodziny(dataczas.getGodziny());
	przesunMinuty(dataczas.getMinuty());
	przesunSekundy(dataczas.getSekundy());

	przesunDzien(dataczas.getDzien());
	przesunMiesiac(dataczas.getMiesiac());
	przesunRok(dataczas.getRok());

}

void DataCzas::obliczDataCzas(DataCzas dataczas)
{
	this->getCzas().obliczRoznice(dataczas.getCzas());
	this->getData().obliczRoznice(dataczas.getData());

}
