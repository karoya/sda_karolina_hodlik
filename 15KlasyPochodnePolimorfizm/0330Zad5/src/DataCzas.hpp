/*
 * DataCzas.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */
#include "Czas.hpp"
#include "Data.hpp"

#ifndef DATACZAS_HPP_
#define DATACZAS_HPP_

class DataCzas: public Data, public Czas  {
private:

public:
	DataCzas();
	DataCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok, unsigned int godziny,unsigned int minuty,unsigned int sekundy);
	DataCzas(Data data, Czas czas);

	void setCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok);
	void setData(unsigned int dzien, unsigned int miesiac, unsigned int rok);
	Czas getCzas() const;
	Data getData() const;
	void wypisz();
	void przesunDataCzas(DataCzas dataczas);
	void obliczDataCzas(DataCzas dataczas);

};

#endif /* DATACZAS_HPP_ */
