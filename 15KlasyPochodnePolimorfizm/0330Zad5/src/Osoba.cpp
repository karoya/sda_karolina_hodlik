/*
 * Osoba.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Osoba.hpp"

Osoba::Osoba()
: mDataUrodzenia(0,0,0),
mPesel(0),
mPlec(nn)
{

}
Osoba::Osoba(string imie, string nazwisko, Data dataUrodzenia, int* pesel, Plec plec)
: mImie(imie),
 mNazwisko(nazwisko),
 mDataUrodzenia(dataUrodzenia),
mPesel(pesel),
mPlec(plec)
{

}

void Osoba::wypiszDane()
{
	std::cout << "Imie: " << mImie << std::endl;
	std::cout << "Nazwisko: " << mNazwisko << std::endl;
	mDataUrodzenia.wypisz();
}
string Osoba::getPesel()
{
	if (mPesel != 0)
	{
		std::string tmp;
		for (int i=0; i<11; i++)
		{
			tmp+='0'+mPesel[i];
		}
	return tmp;
	}
}
void Osoba::setPesel(int* pesel)
{
	if (pesel != 0)
	{
		if(poprawny)
		{
			mPesel = pesel;
			int dzien, miesiac, rok;
			rok = 1900 + 10 * pesel[0] + pesel[1];
			miesiac = 10 *pesel[2] + pesel[3];
			dzien = pesel[4]+pesel[5];
			mDataUrodzenia = Data(dzien, miesiac, rok);
		}
	}
}
