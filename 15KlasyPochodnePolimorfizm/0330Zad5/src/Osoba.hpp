/*
 * Osoba.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef OSOBA_HPP_
#define OSOBA_HPP_
#include <string>
#include <iostream>
#include "Data.hpp"
using namespace std;
enum Plec
	{
		kobieta,
		mezczyzna,
		nn
	};

class Osoba {
private:
	string mImie;
	string mNazwisko;
	Data mDataUrodzenia;
	int* mPesel;
	Plec mPlec;


public:
	Osoba();
	Osoba(string imie, string nazwisko, Data dataUrodzenia, int* pesel, Plec plec);
	void wypiszDane();

	string getPesel();
	void setPesel(int* pesel);

};

#endif /* OSOBA_HPP_ */
