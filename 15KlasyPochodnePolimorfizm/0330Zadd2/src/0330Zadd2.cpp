//============================================================================
// Name        : 0330Zadd2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
#include "DzikieZwierze.hpp"
#include "Kot.hpp"
#include "Pies.hpp"
#include "KotoPies.hpp"

int main() {

	DzikieZwierze* tablica[3];

	Kot kot;
	Pies pies;
	KotoPies kotopies;

	kot.setImie("Mruczek");
	pies.setImie("Burey");
	kotopies.setImie("Hybryda");

	tablica[0] = &kot;
	tablica[1] = &pies;
	tablica[2] = &kotopies;

	for (unsigned int i = 0; i < 3; i++)
	{
		tablica[i]->dajGlos();
	}
	return 0;
}
