/*
 * DzikieZwierze.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "DzikieZwierze.hpp"
#include <iostream>

DzikieZwierze::DzikieZwierze()
:mImie("")
{
	std::cout << "Dzikie Zwierze" << std::endl;

}
DzikieZwierze::DzikieZwierze(std::string imie)
:mImie(imie)
{
	std::cout << "Imie zwierza: " << getImie() << std::cout;

}

const std::string DzikieZwierze::getImie(){
	return mImie;
}

void DzikieZwierze::setImie(std::string imie) {
	mImie = imie;
}

DzikieZwierze::~DzikieZwierze() {
	std::cout << "~Dzikie zwierze " << getImie() << std::endl;
}


