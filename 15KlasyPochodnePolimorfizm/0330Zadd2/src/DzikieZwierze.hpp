/*
 * DzikieZwierze.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef DZIKIEZWIERZE_HPP_
#define DZIKIEZWIERZE_HPP_
#include <string>

class DzikieZwierze {
private:
	std::string mImie;
public:
	DzikieZwierze();
	DzikieZwierze(std::string imie);
	virtual ~DzikieZwierze();
	const std::string getImie();
	void setImie(std::string imie);
	virtual void dajGlos() = 0;

};

#endif /* DZIKIEZWIERZE_HPP_ */
