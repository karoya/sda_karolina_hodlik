/*
 * Kot.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Kot.hpp"
#include <iostream>

Kot::Kot() {
	std::cout << "Kot " << getImie() << std::endl;
}

Kot::~Kot() {
	std::cout << "~Kot" << std::endl;
}

void Kot::dajGlos()
{
	std::cout << "Miau" <<std::endl;
}
