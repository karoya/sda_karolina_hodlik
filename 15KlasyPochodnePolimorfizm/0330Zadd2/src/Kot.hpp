/*
 * Kot.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef KOT_HPP_
#define KOT_HPP_
#include "DzikieZwierze.hpp"

class Kot: virtual public DzikieZwierze {
public:
	Kot();
	~Kot();
	void dajGlos();

};

#endif /* KOT_HPP_ */
