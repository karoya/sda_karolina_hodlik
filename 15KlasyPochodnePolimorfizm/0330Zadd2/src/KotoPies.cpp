/*
 * KotoPies.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "KotoPies.hpp"
#include <iostream>

KotoPies::KotoPies() {
	std::cout << "KotoPies " << getImie() << std::endl;

}

KotoPies::~KotoPies() {
	std::cout << "~KotoPies " << getImie() << std::endl;
}

void KotoPies::dajGlos()
{
	std::cout << "MiauHau" << std::endl;
}
