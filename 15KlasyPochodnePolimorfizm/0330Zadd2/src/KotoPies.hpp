/*
 * KotoPies.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef KOTOPIES_HPP_
#define KOTOPIES_HPP_
#include "Pies.hpp"
#include "Kot.hpp"

class KotoPies: public Kot, public Pies {
public:
	KotoPies();
	~KotoPies();
	void dajGlos();
};

#endif /* KOTOPIES_HPP_ */
