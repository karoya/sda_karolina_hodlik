/*
 * Pies.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef PIES_HPP_
#define PIES_HPP_
#include "DzikieZwierze.hpp"

class Pies: virtual public DzikieZwierze {
public:
	Pies();
	~Pies();
	void dajGlos();
};

#endif /* PIES_HPP_ */
