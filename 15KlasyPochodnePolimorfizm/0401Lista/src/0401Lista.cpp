#include "Lista1.hpp"
#include <iostream>

int main()
{
	Lista lista;

	lista.dodajPoczatek(23);
	lista.wypisz();
	lista.dodajKoniec(4);
	lista.wypisz();
	std::cout << "Znaleziona wartosc " << lista.pobierz(2) << std::endl;
}
