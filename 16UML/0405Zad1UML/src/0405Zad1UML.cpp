//============================================================================
// Name        : 0405Zad1UML.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

class Time
{
private:
	int mMinute;
	int mSecond;
	int mHour;

public:
	Time(int second, int minute, int hour);
	int getHour();
	int getMinute();
	int getSecond();
	void setHour(int hour);
	void setMinute(int minute);
	void setSecond(int second);
	void setTime(int hour, int minute, int second);
	std::string toString();
	Time nextSecond();
	Time previousSecond();
};
class MyTriangle
{
	class MyPoint
	{
	private:
		int x;
		int y;
	};
private:
	MyPoint mV1;
	MyPoint mV2;
	MyPoint mV3;
public:
	MyTriangle(int x1, int y1, int x2, int y2, int x3, int y3);
	MyTriangle(MyPoint v1, MyPoint v2, MyPoint v3);
	std::string toString();
	double getPermiter();
	std::string getType();
};
class Circle
{
private:
	double mRadius = 1.0;
	std::string mColor = "red";
public:
	Circle();
	Circle(double radius);
	Circle(double radius, std::string color);
	double getRadius();
	void setRadius(double radius);
	std::string getColor();
	void setColor(std::string color);
	double getArea();
	std::string toString();
};
class Cylinder: public Circle
{
private:
	double mHeight = 1.0;
public:
	Cylinder();
	Cylinder(double radius);
	Cylinder(double radius, double height);
	Cylinder(double radius, double height, std::string color);
	double getHeight();
	void setHeight(double height);
	double getVolume();
};
int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	return 0;
}
