/*
 * Zmywarka.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef ZMYWARKA_HPP_
#define ZMYWARKA_HPP_

#include <iostream>
using namespace std;
class WashOption {
private:
	int mWashSelection;
public:
	int getwashSelection()
	{
		return mWashSelection;
	};

};
class Timer {
private:
	int mValue = 1;
	int mDuration = 1;
public:
	void serDuration(int in)
	{

	};
	void start();
	int count();
	int getValue();
	int getDuration();
};
class Engine {
private:
	int mRotation = 1;
};
class Machine {

public:
	virtual ~Machine();
	virtual void turnOn() = 0;
	virtual void turnOff() = 0;
};
class WashingMachine: public Machine {
private:
	int mWashTime = 1;
	int mRinseTime = 1;
	int mSpinTime = 1;

	void turnOn()
	{

	};
	void turnOff()
	{

	};
	void wash();
	void rinse();
	void spin()
	{
		Engine engine;
		Timer timer;
		engine.turnOn();
		timer.setDuration(60*60);

		timer.start();
		int time = timer.getValue();
		int duration = getDuration();

		while (time!=duration){
			time = time.count()l
		}

		engine.turnOff();

	};
	void fill();
	void empty();
	void standardWash();
	void twinceRinse();
public:
	void main() {
		WashOption washOption;
		switch (washOption.getwashSelection()) {
		case 1:
			standardWash();
			break;
		case 2:
			twinceRinse();
			break;
		case 3:
			spin();
			break;
		default:
			break;
		}
	}
};


class Sensor {
public:
	virtual ~Sensor();
	virtual bool check() = 0;
};
class DoorSensor: public Sensor {
public:
	bool check();
};
class TempSensor: public Sensor {
public:
	bool check();
};
class WaterSensor: public Sensor {
public:
	int mCurrentLevel = 1;
	int mDesiredLevel = 1;
	bool check();
};

#endif /* ZMYWARKA_HPP_ */
