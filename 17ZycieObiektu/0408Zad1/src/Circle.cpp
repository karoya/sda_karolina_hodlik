/*
 * Circle.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "Circle.hpp"
#include <iostream>
#include <string>

Circle::Circle(float r, std::string nazwa, float pi)
:mR(r),
mNazwa(nazwa),
mPi(pi = 3.14){
	std::cout << "Circle" <<std::endl;

}

std::string Circle::getNazwa() {
	return mNazwa;
}

void Circle::setNazwa(std::string nazwa) {
	mNazwa = nazwa;
}

float Circle::getPi(){
	return mPi;
}

float Circle::getR(){
	return mR;
}

void Circle::setR(float r) {
	mR = r;
}

Circle::~Circle() {
	std::cout << "~Circle" <<std::endl;
}
std::string Circle::nazwa()
{
	std::cout << "Nazwa" << getNazwa() << std::endl;
	return mNazwa;
}
float Circle::pole()
{
	float pole = getR()* getR() * getPi();
	std::cout << "Pole kola to: " << pole << std::endl;
	return pole;
}
