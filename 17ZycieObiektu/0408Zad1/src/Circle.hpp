/*
 * Circle.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include "Shape.hpp"
#include <string>

#ifndef CIRCLE_HPP_
#define CIRCLE_HPP_

class Circle: public Shape {
private:
	float mR;
	std::string mNazwa;
	const float mPi;
public:
	Circle(float r, std::string nazwa, float pi);
	~Circle();
	float pole();
	std::string getNazwa();
	void setNazwa(std::string nazwa);
	float getPi();
	float getR();
	void setR(float r);
	std::string nazwa();
};

#endif /* CIRCLE_HPP_ */
