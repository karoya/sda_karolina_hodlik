/*
 * Cylinder.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "Cylinder.hpp"
#include <iostream>

Cylinder::Cylinder(float r, std::string nazwa, float pi, float h)
: Circle(r, nazwa, pi = 3.14),
  mH(h)
{
	std::cout << "Cylinder" <<std::endl;

}

float Cylinder::getH(){
	return mH;
}

void Cylinder::setH(float h) {
	mH = h;
}

Cylinder::~Cylinder() {
	std::cout << "~Cylinder" <<std::endl;
}

std::string Cylinder::nazwa()
{
	std::cout << "Nazwa" << getNazwa() << std::endl;
	return getNazwa();
}
float Cylinder::pole()
{
	float pole = (2*getR()*getR()*getPi()) + (2*getR()*getPi()*getH());
	std::cout << "Pole kola to: " << pole << std::endl;
	return pole;
}
