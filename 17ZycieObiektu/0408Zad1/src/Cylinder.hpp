/*
 * Cylinder.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include "Circle.hpp"
#ifndef CYLINDER_HPP_
#define CYLINDER_HPP_

class Cylinder: public Circle {
private:
	float mH;
public:
	Cylinder(float r, std::string nazwa, float pi, float h);
	~Cylinder();
	float pole();
	std::string nazwa();
	float getH();
	void setH(float h);
};

#endif /* CYLINDER_HPP_ */
