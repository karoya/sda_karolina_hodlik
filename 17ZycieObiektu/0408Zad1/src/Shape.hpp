/*
 * Shape.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include <string>
#ifndef SHAPE_HPP_
#define SHAPE_HPP_

class Shape {
public:
	Shape();
	virtual ~Shape();
	virtual float pole() = 0;
	virtual std::string nazwa() = 0;
};

#endif /* SHAPE_HPP_ */
