/*
 * Nominaly.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include "Automat.hpp"
#include <iostream>
Nominaly::Nominaly(float kwota, float gotowka)
: mKwota(kwota),
 mGotowka(gotowka)
 {

}

float Nominaly::getGotowka(){
	return mGotowka;
}

void Nominaly::setGotowka(float gotowka) {
	mGotowka = gotowka;
}

float Nominaly::getKwota(){
	return mKwota;
}

void Nominaly::setKwota(float kwota) {
	mKwota = kwota;
}

float* Nominaly::getNominaly(){
	return mNominaly;
}

Nominaly::~Nominaly() {

}

float Nominaly::ObliczReszte()
{
	float tmp = getGotowka() - getKwota();
	std::cout << "Reszta " << tmp << std::endl;
	int i = 0;
	while (tmp > 0 && i < 15)
	{
		if (tmp >= getNominaly()[i])
		{
			int ileRazyWydac = tmp/getNominaly()[i];
			tmp = tmp - (getNominaly()[i]*ileRazyWydac);

			std::cout << "Nominal " << getNominaly()[i] << " x " << ileRazyWydac << std::endl;

		}
		i++;


	}
	return 0.0;
}

