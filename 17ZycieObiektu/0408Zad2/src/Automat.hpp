/*
 * Nominaly.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef AUTOMAT_HPP_
#define AUTOMAT_HPP_

class Nominaly {
private:
	float mNominaly[15] =  {500.0, 200.0, 100.0, 50.0, 20.0, 10.0, 5.0, 2.0, 1.0, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01};
	float mKwota;
	float mGotowka;

public:
	Nominaly(float kwota, float gotowka);
	~Nominaly();
	float getGotowka();
	void setGotowka(float gotowka);
	float getKwota();
	void setKwota(float kwota);
	float* getNominaly();
	float ObliczReszte();

};

#endif /* AUTOMAT_HPP_ */
