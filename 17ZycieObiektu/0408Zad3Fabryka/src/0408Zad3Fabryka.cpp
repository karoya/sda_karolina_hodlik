//============================================================================
// Name        : 0408Zad3Fabryka.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;
class Procesor {
public:
	Procesor()
{

}
	virtual ~Procesor() {
	}
	virtual void process() = 0;
};
class ProcesorAMD: public Procesor {
public:
	ProcesorAMD() {
	}
	~ProcesorAMD() {
	}
	void process() {
	}
};
class ProcesorIntel: public Procesor {
public:
	ProcesorIntel() {
	}
	~ProcesorIntel() {
	}
	void process() {
	}
};

class Cooler {
public:
	virtual ~Cooler() {
	}
	virtual void cool() = 0;
};
class CoolerAMD: public Cooler {
public:
	CoolerAMD() {
	}
	~CoolerAMD() {
	}
	void cool() {
	}
};
class CoolerIntel: public Cooler {
public:
	CoolerIntel() {

	}
	~CoolerIntel() {

	}
	void cool() {

	}
};

class AbstractSemiconductorFactory {
public:
	virtual ~AbstractSemiconductorFactory() {

	}
	virtual Procesor* createProcessor() = 0;
	virtual Cooler* createCooler() = 0;

};

class IntelSemiconductorFactory: public AbstractSemiconductorFactory {
public:
	IntelSemiconductorFactory() {

	}
	~IntelSemiconductorFactory() {

	}
	Procesor* createProcessor() {
		return new ProcesorIntel();
	}
	Cooler* createCooler() {
		return new CoolerIntel();
	}

};
class AMDSemiconductorFactory: public AbstractSemiconductorFactory {
public:
	AMDSemiconductorFactory() {

	}
	~AMDSemiconductorFactory() {

	}
	Procesor* createProcessor() {
		return new ProcesorAMD();
	}
	Cooler* createCooler() {
		return new CoolerAMD();
	}

};
class Computer {
private:
	std::string mName;
	Procesor* mProcesor;
	Cooler* mCooler;

public:
	Computer(std::string name, AbstractSemiconductorFactory& factory) :
			mName(name) {
		mProcesor = factory.createProcessor();
		mCooler = factory.createCooler();
	}
	~Computer() {
		delete mProcesor;
		delete mCooler;
	}
	;

	void run() {
		std::cout << "Nazywam sie " << mName << std::endl;
		mProcesor->process();
		mCooler->cool();
	}
	;
};

int main() {
	IntelSemiconductorFactory intelFactory;
	AMDSemiconductorFactory amdFactory;

	Computer intelPC("PC1", intelFactory);
	Computer AMDPC("PC2", amdFactory);

	intelPC.run();
	AMDPC.run();

	return 0;
}
