/*
 * Plik.cpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#include "Plik.hpp"

Plik::Plik(std::string filename, int log_level = 1)
:file(filename.c_str(), std::ofstream::out),
 level(log_level)
{


}

Plik::~Plik() {
file.close();
}

void Plik::log(std::string message, int log_level = 3) {

	if(level < log_level) return;

	time_t t;

	t = time(NULL);

	struct tm* current = localtime(&t);

	file << "[" << current->tm_year + 1900 << " " << current->tm_mon +1 << " " <<  current->tm_mday << "] " << message << std::endl;
	//file << current->tm_year + 1900 << " " << current->tm_mon +1 << " " << message;

}

void Plik::log(int a) {

	time_t t;

	t = time(NULL);

	struct tm* current = localtime(&t);

	file << "[" << current->tm_year + 1900 << " " << current->tm_mon +1 << " " <<  current->tm_mday << "] " << a << std::endl;
	//file << current->tm_year + 1900 << " " << current->tm_mon +1 << " " << message;

}
