/*
 * Plik.hpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */
#include <fstream>
#include <string>
#include <ctime>

#ifndef PLIK_HPP_
#define PLIK_HPP_

class Plik {

private:

	std::ofstream file;
	int level;

public:
	Plik(std::string filename, int log_level = 1);
	~Plik();

	void log(std::string message, int log_level);
	void log(int a);
};

#endif /* PLIK_HPP_ */
