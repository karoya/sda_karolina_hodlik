#include <iostream>
using namespace std;
#include "Punkt.hpp"
#include "Zbior.hpp"

int main()
{

	Punkt p1(3, 6);
	Punkt p2 = p1;
	Punkt p3 = p2++ + 3;


	//cout << p2.getX() << " " << p2.getY() << endl;
	//Punkt pkt3 = 2.5f + p2;
//	Punkt p3(0, 0);
//	p3 = p1 * p2;


	Zbior zbior;
	if (zbior)
	{
		std::cout<<"Prawidłowy" <<std::endl;
	}
	else
	{
		std::cout<<"Nieprawidłowy" <<std::endl;
	}
	zbior + p1;
	zbior + p2;
	zbior + p3;
	zbior.wypisz();


	Zbior zbiornik;
	zbiornik + Punkt(33, 33);
	zbiornik + Punkt(11, 11);
	zbior + zbiornik;
	zbior.wypisz();

	return 0;

}
