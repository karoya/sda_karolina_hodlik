//============================================================================
// Name        : 0508OperatoryIILiquid.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Liquid.hpp"
#include "Rum.hpp"
#include "Cup.hpp"
#include "Decaff.hpp"
#include "Milk.hpp"
#include "Espresso.hpp"
#include "Coffee.hpp"


using namespace std;

int main() {
	Cup kubekKawyZMlekiem;
	kubekKawyZMlekiem.add(new Decaff(300, 0));
	kubekKawyZMlekiem.add(new Rum(250,Rum::Dark));
	kubekKawyZMlekiem.add(new Milk(100, 2.0));

	kubekKawyZMlekiem.wypisz();

	return 0;
}
