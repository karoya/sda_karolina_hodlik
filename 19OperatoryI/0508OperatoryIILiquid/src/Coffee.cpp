/*
 * Coffee.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "Coffee.hpp"

Coffee::Coffee(int amount, int caffeine) :
		Liquid(amount), mCaffeine(caffeine)
{
	// TODO Auto-generated constructor stub

}

int Coffee::getCaffeine() const
{
	return mCaffeine;
}

void Coffee::setCaffeine(int caffeine)
{
	mCaffeine = caffeine;
}

Coffee::~Coffee()
{

}

void Coffee::add(int a)
{
	mAmount += a;
}
void Coffee::remove(int a)
{
	if (mAmount < a)
	{
		mAmount = 0;
	}
	else
	{
		mAmount -= a;
	}
}
void Coffee::removeAll()
{
	mAmount = 0;
}
