/*
 * Coffee.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef COFFEE_HPP_
#define COFFEE_HPP_
#include "Liquid.hpp"

class Coffee:public Liquid {
private:
	int mCaffeine;
public:
	Coffee(int amount, int caffeine);
	~Coffee();
	virtual void add(int a);
	virtual void remove(int a);
	virtual void removeAll();
	int getCaffeine() const;
	void setCaffeine(int caffeine);
};

#endif /* COFFEE_HPP_ */
