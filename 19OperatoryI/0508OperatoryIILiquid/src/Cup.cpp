/*
 * Cup.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "Cup.hpp"
#include <iostream>
Cup::Cup():
mContent(0), mSize(0)
{

}

Cup::~Cup() {
	if (mSize != 0)
	{
		for(size_t i = 0; i<mSize; i++)
		{
			delete mContent[i];
		}
		delete [] mContent;
		mSize = 0;
	}
}

void Cup::add(Liquid* liquid)
{
	resize();
	mContent[mSize-1] = liquid; // poprawic bo niezgodne z RAII, sprawdzic czy nie duplikujemy plynow
}
void Cup::add(const Milk &liquid)
{
	resize();
	mContent[mSize-1] = new Milk(liquid);
}
void Cup::add(const Coffee &liquid)
{
	resize();
	mContent[mSize-1] = new Coffee(liquid);
}
void Cup::takeSip(int a)
{
	int liquidAmount = calculateLiquidAmount();
	if(liquidAmount > 0 && a > 0)
	{
		if (liquidAmount <= a)
		{
			spill();
		}
		else
		{
			for (size_t i =0; i<mSize; i++)
			{
				int liquidRatio = liquidAmount / mContent[i] -> getAmount();
				mContent[i]->remove(liquidRatio*a);
			}
		}
	}
	else
	{
// do nothing
	}
}
void Cup::spill()
{
	for (size_t i =0; i<mSize; i++)
	{
		mContent[i]->removeAll();

	}
	clear();
}
void Cup::resize()
{
	if(mSize==0)
			{
				mContent = new Liquid*[++mSize];
			}
	else
	{
		Liquid** tmp = new Liquid*[++mSize]; // nowa tymczasowa tablica wskaznikow do ktorej przypisujemy nowa dynamiczna tablie
		for (size_t i = 0; i<mSize; i++) // kopiowanie ze starej tablicy do nowej
		{
			tmp[i] = mContent[i];
		}
		delete [] mContent;
		mContent = tmp;//podmieniamy tablice
	}


}
void Cup::clear()
{
	for (size_t i =0; i<mSize; i++)
		{
			delete mContent;
		}
		delete[] mContent;
		mContent = 0;
		mSize = 0;

}
int Cup::calculateLiquidAmount()
{
	int liquidAmount = 0;
	if(mSize ==0)
	{
		//Do sth
	}
	else
	{

		for (size_t i =0; i<mSize; i++)
		{
			liquidAmount +=  mContent[i]->getAmount();
		}
	}
	return liquidAmount;

}

void Cup::wypisz()
{
	for (size_t i =0; i<mSize; i++)
		{
			std::cout<< "["<<mContent[i]->getAmount()<<"]"<<std::endl;
		}
}
