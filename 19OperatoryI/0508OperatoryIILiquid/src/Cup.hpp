/*
 * Cup.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CUP_HPP_
#define CUP_HPP_
#include <cstdlib>
#include "Liquid.hpp"
class Cup {
private:
	Liquid** mContent;
	size_t mSize;
	void clear();
	int calculateLiquidAmount();
public:
	Cup();
	~Cup();
	void wypisz();
	void add(Liquid* liquid);
	void add(const Milk &liquid);
	void add(const Coffee &liquid)
	void takeSip(int amount);
	void spill();
	void resize();
	//opertor +=
};

#endif /* CUP_HPP_ */
