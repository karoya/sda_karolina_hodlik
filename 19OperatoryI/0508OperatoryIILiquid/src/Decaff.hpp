/*
 * Decaff.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef DECAFF_HPP_
#define DECAFF_HPP_
#include "Coffee.hpp"

class Decaff: public Coffee {
public:
	Decaff(int amount, int caffeine);
	~Decaff();
	virtual void add(int a);
	virtual void remove(int a);
	virtual void removeAll();
};

#endif /* DECAFF_HPP_ */
