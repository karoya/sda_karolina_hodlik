/*
 * Espresso.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef ESPRESSO_HPP_
#define ESPRESSO_HPP_
#include "Coffee.hpp"

class Espresso: public Coffee {
public:
	Espresso(int amount, int caffeine);
	~Espresso();
	virtual void add(int a);
	virtual void remove(int a);
	virtual void removeAll();
};

#endif /* ESPRESSO_HPP_ */
