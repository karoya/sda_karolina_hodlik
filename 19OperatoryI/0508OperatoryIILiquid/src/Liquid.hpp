/*
 * Liquid.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef LIQUID_HPP_
#define LIQUID_HPP_

class Liquid {
protected:
	int mAmount;
public:
	Liquid(int amount);
	virtual ~Liquid();
	int getAmount() const;

	virtual void add(int a) = 0;
	virtual void remove(int a) = 0;
	virtual void removeAll() = 0;
};

#endif /* LIQUID_HPP_ */
