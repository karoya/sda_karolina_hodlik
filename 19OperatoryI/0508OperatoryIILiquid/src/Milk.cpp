/*
 * Milk.cpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#include "Milk.hpp"

Milk::Milk(int amount, float fat):
Liquid(amount),
mFat(fat)
{

}
Milk::~Milk() {

}
float Milk::getFat() const {
	return mFat;
}

void Milk::setFat(float fat) {
	mFat = fat;
}

void Milk::add(int a)
{
	mAmount += a;
}
void Milk::remove(int a)
{
	if (mAmount < a)
{
	mAmount = 0;
}
else
{
	mAmount -= a;
}
}
void Milk::removeAll()
{
	mAmount = 0;
}
