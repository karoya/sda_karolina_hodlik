/*
 * Milk.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef MILK_HPP_
#define MILK_HPP_
#include "Liquid.hpp"

class Milk: public Liquid {
private:
	float mFat;
public:
	Milk(int amount, float fat);
	~Milk();
	virtual void add(int a);
	virtual void remove(int a);
	virtual void removeAll();
	float getFat() const;
	void setFat(float fat);
};

#endif /* MILK_HPP_ */
