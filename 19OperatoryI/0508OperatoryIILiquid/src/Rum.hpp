/*
 * Rum.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef RUM_HPP_
#define RUM_HPP_
#include "Liquid.hpp"

class Rum: public Liquid {

public:
	enum Colour
	{
		Dark,
		Light
	};
	Rum(int amount, Colour kolor);
	~Rum();
	virtual void add(int a);
	virtual void remove(int a);
	virtual void removeAll();


private:
	Colour mKolor;
};

#endif /* RUM_HPP_ */
