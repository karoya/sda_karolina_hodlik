
#include <iostream>
#include "Czlowiek.hpp"
#include "Student.hpp"
#include "Pracownik.hpp"

int main()
{
	//Czlowiek czlowiek = {3,4,1920}; - bez konstruktora
	//Czlowiek czlowiek(3,4,1920);
//	czlowiek.mRokUr = 2050;
//	czlowiek.podajWiek(2070);
	Pracownik pracus;
	pracus.mPensja = 1300;
	pracus.mDzienUr = 21;
	pracus.mMiesiacUr = 11;
	pracus.mRokUr = 1990;
	pracus.mStaz = 0;

	pracus.podajWiek(2017);
	pracus.wyliczPensje();
	return 0;
}
