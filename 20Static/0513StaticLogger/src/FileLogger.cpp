/*
 * FileLogger.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "FileLogger.hpp"

FileLogger* FileLogger::instance = 0;

FileLogger::FileLogger()
{
	mFile.open("log.txt");
}

void FileLogger::log(string DataToLog)
{
	mFile<<DataToLog<<std::endl;
}

//ofstream FileLogger::getFile() {
//	return mFile;
//}
//
//void FileLogger::setFile(ofstream file) {
//	mFile = file;
//}

FileLogger* FileLogger::instance_()
{
	if(!instance)
		instance = new FileLogger;
	return instance;
}
