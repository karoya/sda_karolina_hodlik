/*
 * FileLogger.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */
#include <iostream>
#include <fstream>
#include <string>
#ifndef FILELOGGER_HPP_
#define FILELOGGER_HPP_

using namespace std;

class FileLogger {
private:
	ofstream mFile;
	static FileLogger *instance;
	FileLogger();
public:
	void log(string DataToLog);
//	ofstream getFile();
//	void setFile(ofstream file);
	static FileLogger* instance_();
};

#endif /* FILELOGGER_HPP_ */
