/*
 * Kalkulator.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef KALKULATOR_HPP_
#define KALKULATOR_HPP_

class Kalkulator
{
public:
	int podziel(int a, int b)
	{

		FileLogger::instance_()->log("Kalkulator::podziel");
		if(b ==0)
		{
			FileLogger::instance_()->log("Nie dzieli sie przez 0!");
		return 0;
		}
		else
		{
			return a/b;
		}
	}
};




#endif /* KALKULATOR_HPP_ */
