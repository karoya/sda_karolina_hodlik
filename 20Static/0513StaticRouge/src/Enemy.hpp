/*
 * Enemy.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef ENEMY_HPP_
#define ENEMY_HPP_

#include "GameObject.hpp"

class Enemy : public GameObject
{
public:
	Enemy()
	: GameObject()
	{}

	Enemy(int x, int y, char symbol)
	: GameObject(x, y, symbol)
	{}

	virtual void update()
	{

	}
};

#endif /* ENEMY_HPP_ */
