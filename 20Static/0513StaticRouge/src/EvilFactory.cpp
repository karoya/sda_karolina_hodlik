/*
 * EvilFactory.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "EvilFactory.hpp"
#include "Skeleton.hpp"
#include "Zombie.hpp"
#include "Snail.hpp"
#include "Rat.hpp"

EvilFactory::EvilFactory()
{
	// TODO Auto-generated constructor stub

}

Undead* EvilFactory::createUndead(UndeadType::UndeadID id, int x, int y)
{
	switch(id)
	{
	case UndeadType::Skeleton:
		return new Skeleton(x, y);
		break;
	case UndeadType::Zombie:
		return new Zombie(x, y);
		break;
	default:
		return 0;
	}
}

Animal* EvilFactory::createAnimal(AnimalType::AnimalID id, int x, int y)
{
	switch(id)
		{
		case AnimalType::Rat:
			return new Rat(x, y);
			break;
		case AnimalType::Snail:
			return new Snail(x, y);
			break;
		default:
			return 0;
		}
}
