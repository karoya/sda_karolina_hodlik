/*
 * EvilFactory.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef EVILFACTORY_HPP_
#define EVILFACTORY_HPP_

#include "Animal.hpp"
#include "Undead.hpp"

	namespace UndeadType
	{
		enum UndeadID
		{
			Zombie,
			Skeleton
		};
	}

	namespace AnimalType
	{
		enum AnimalID
		{
			Rat,
			Snail
		};
	}

class EvilFactory
{
public:
	static Undead* createUndead(UndeadType::UndeadID id, int x, int y);
	static Animal* createAnimal(AnimalType::AnimalID id, int x, int y);

private:
	EvilFactory();
};

#endif /* EVILFACTORY_HPP_ */
