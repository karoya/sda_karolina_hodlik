/*
 * GameObject.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef GAMEOBJECT_HPP_
#define GAMEOBJECT_HPP_

class GameObject
{
private:
	int mX;
	int mY;
	char mSymbol;

public:
	virtual void update() =0;

	virtual ~GameObject()
	{

	}

	GameObject()
	: mX(0)
	, mY(0)
	, mSymbol('?')
	{
	}

	GameObject(int x, int y, char symbol)
	: mX(x)
	, mY(y)
	, mSymbol(symbol)
	{
	}

	int getX() const
	{
		return mX;
	}

	void setX(int x)
	{
		mX = x;
	}

	int getY() const
	{
		return mY;
	}

	void setY(int y)
	{
		mY = y;
	}

	char getSymbol() const
	{
		return mSymbol;
	}
};



#endif /* GAMEOBJECT_HPP_ */
