/*
 * Rat.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "Rat.hpp"

#include <iostream>

Rat::Rat()
: Animal()
{
}

Rat::Rat(int x, int y)
: Animal(x, y, 'q')
{
}

Rat::~Rat()
{
}

void Rat::makeNoise()
{
	std::cout<<"rat"<<std::endl;
}
