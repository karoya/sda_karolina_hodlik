/*
 * Rat.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef RAT_HPP_
#define RAT_HPP_

#include "Animal.hpp"

class Rat: public Animal
{
public:
	Rat();
	Rat(int x, int y);

	virtual ~Rat();

	void makeNoise();
};

#endif /* RAT_HPP_ */
