//============================================================================
// Name        : RogueGame.cpp
// Author      : bla
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include "EvilFactory.hpp"
#include "Skeleton.hpp"
#include "Zombie.hpp"
#include "Snail.hpp"
#include "Rat.hpp"
#include "Undead.hpp"
#include "Animal.hpp"

using namespace std;

int main() {
	Animal* anim = EvilFactory::createAnimal(AnimalType::Snail, 1 ,2);
	anim->makeNoise();
	anim = EvilFactory::createAnimal(AnimalType::Rat , -1, 10);
	anim->makeNoise();

	Undead* undead = EvilFactory::createUndead(UndeadType::Skeleton, 0 , 7);
	undead->speak();
	undead = EvilFactory::createUndead(UndeadType::Zombie , 9, 9);
	undead->speak();

	return 0;
}
