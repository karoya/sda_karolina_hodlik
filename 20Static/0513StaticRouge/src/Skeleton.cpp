/*
 * Skeleton.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include <iostream>

#include "Skeleton.hpp"

Skeleton::Skeleton()
: Undead()
{
}

Skeleton::Skeleton(int x, int y)
: Undead(x, y, 'S')
{
}

Skeleton::~Skeleton()
{
}

void Skeleton::speak()
{
	std::cout<<"skeleton"<<std::endl;
}
