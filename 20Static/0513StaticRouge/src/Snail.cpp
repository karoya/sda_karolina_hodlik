/*
 * Snail.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "Snail.hpp"
#include <iostream>

Snail::Snail()
: Animal()
{
}

Snail::Snail(int x, int y)
: Animal(x, y, '@')
{
}

Snail::~Snail()
{
}

void Snail::makeNoise()
{
	std::cout<<"snail"<<std::endl;
}
