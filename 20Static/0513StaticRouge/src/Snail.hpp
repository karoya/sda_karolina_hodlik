/*
 * Snail.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef SNAIL_HPP_
#define SNAIL_HPP_

#include "Animal.hpp"

class Snail: public Animal
{
public:
	Snail();
	Snail(int x, int y);

	virtual ~Snail();

	void makeNoise();
};

#endif /* SNAIL_HPP_ */
