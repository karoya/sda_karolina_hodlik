/*
 * Undead.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef UNDEAD_HPP_
#define UNDEAD_HPP_

#include "Enemy.hpp"

class Undead : public Enemy
{
public:
	Undead()
	: Enemy()
	{
	}

	Undead(int x, int y, char symbol)
	: Enemy(x, y, symbol)
	{
	}

	virtual void speak() =0;

	virtual ~Undead()
	{
	}

private:

};

#endif /* UNDEAD_HPP_ */
