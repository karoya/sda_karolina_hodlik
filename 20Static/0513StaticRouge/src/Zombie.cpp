/*
 * Zombie.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "Zombie.hpp"

#include <iostream>

Zombie::Zombie()
: Undead()
{
}

Zombie::Zombie(int x, int y)
: Undead(x, y, 'z')
{
}

Zombie::~Zombie()
{
}

void Zombie::speak()
{
	std::cout<<"zombie"<<std::endl;
}
