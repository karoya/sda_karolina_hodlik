/*
 * Zombie.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef ZOMBIE_HPP_
#define ZOMBIE_HPP_

#include "Undead.hpp"

class Zombie : public Undead
{
public:
	Zombie();
	Zombie(int x, int y);
	virtual ~Zombie();

	void speak();

private:
};

#endif /* ZOMBIE_HPP_ */
