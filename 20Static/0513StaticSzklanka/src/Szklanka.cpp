/*
 * Szklanka.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "Szklanka.hpp"

Szklanka::Szklanka(float pojemnosc)
:mPojemnosc(pojemnosc)
//: cena(11) - nie mozna inicjalizowac statycznych danych w konstruktorze
{
	//cena = 12; przy tworzeniu nowego obiektu cena znowu zmieni sie na 12 wiec najlepiej jej nie inicjowac w konstruktorze
}

int Szklanka::cena = 20; //mozna przypisac wartosc

int Szklanka::getCena(){
	return cena;
}

void Szklanka::setCena(int cc) {
	cena = cc;
}

Szklanka::~Szklanka() {
	// TODO Auto-generated destructor stub
}

float Szklanka::getPojemnosc() const {
	return mPojemnosc;
}

void Szklanka::setPojemnosc(float pojemnosc) {
	mPojemnosc = pojemnosc;
}
