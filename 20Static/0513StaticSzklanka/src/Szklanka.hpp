/*
 * Szklanka.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef SZKLANKA_HPP_
#define SZKLANKA_HPP_

class Szklanka {
private:
	static int cena;
	float mPojemnosc;
public:
	Szklanka(float pojemnosc);
	~Szklanka();
	static int getCena();
	static void setCena(int cc);
	float getPojemnosc() const;
	void setPojemnosc(float pojemnosc);
};

#endif /* SZKLANKA_HPP_ */
