#ifndef PASSWORD_HPP_
#define PASSWORD_HPP_
#include <string>
class Password {

public:

	int mLength;
	Password(int len) :
			mLength(len) {
	}
	char rollDigit() {
		return genChar('0', 10);
	}
	char rollUpper() {
		return genChar('A', 26);
	}

	char rollLower() {
		return genChar('a', 26);
	}

	std::string genPasswd() {
		std::string pwd;
		pwd += rollDigit();
		pwd += rollUpper();
		pwd += rollLower();

		for (int i = 3; i < mLength; i++) {
			int x = rand() % 3;
			if (x == 0) {
				pwd += rollDigit();
			}

			else if (x == 1) {
				pwd += rollUpper();
			}

			else {
				pwd += rollLower();
			}
		}

		mixString(pwd);
		return pwd;
	}

	void mixString(std::string& str) {
		for (int i = str.length(); i > 0; i--) {
			int pos = rand() % str.length();
			char tmp = str[i - 1];
			str[i - 1] = str[pos];
			str[pos] = tmp;
		}
	}

private:
	char genChar(char begin, int range) {
		return begin + rand() % range;
	}
};

class Math {
public:
	int mPierwsza;
	int mDruga;

	Math(int pierwsza, int druga) :
			mPierwsza(pierwsza), mDruga(druga) {

	}

	int sum() {
		return mPierwsza + mDruga;
	}

};
class Math2 {
public:
	float mTrzecia;
	float mCzwarta;

	Math2(float trzecia, float czwarta) :
			mTrzecia(trzecia), mCzwarta(czwarta) {

	}
	int multiplication() {
		return mTrzecia * mCzwarta;
	}

};
class Rzymskie {

public:
	std::string mRzym;

	Rzymskie(std::string rzym) :
			mRzym(rzym) {

	}

	int zamiana() {
		int wynik = 0;
		for (unsigned i = 0; i < mRzym.length(); ++i) {
			char znak = mRzym[i];
			char nastepnyZnak = (i + 1 < mRzym.length()) ? mRzym[i + 1] : '\0';
			if (znak == 'I' && nastepnyZnak == 'V') {
				wynik = wynik - 1;
			} else if (znak == 'I' && nastepnyZnak == 'X') {
				wynik = wynik - 1;
			} else if (znak == 'I') {
				wynik = wynik + 1;
			} else if (znak == 'V') {
				wynik = wynik + 5;
			}

		}
		return wynik;
	}
	};

#endif /* PASSWORD_HPP_ */
