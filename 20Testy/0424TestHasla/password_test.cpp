#include "gtest/gtest.h"

#include "Password.hpp"

TEST(PwdTest, passwordGen)
{
	Password pwdGen(7);
	std::string password = pwdGen.genPasswd();

	ASSERT_EQ(7u, password.length());
	ASSERT_TRUE(password.find_first_of("0123456789") != std::string::npos);
	ASSERT_TRUE(password.find_first_of("abcdefghijklmnoprstuqwxyz") != std::string::npos);
}

TEST(MathTest, SumTest)
{
	Math math(2, 4);

	EXPECT_EQ(6, math.sum());

}
TEST(MathTest, MultiplicationTest)
{
	Math2 math2(2, 4);

	EXPECT_EQ(8, math2.multiplication());

}

TEST(RzymTest, ZamianaTest)
{
	Rzymskie rzym("II");

	EXPECT_EQ(2, rzym.zamiana());

}
/*TEST(PwdTest, passwordGen1)
{
	Password pwdGen(7);
	std::string password = pwdGen.genPasswd();

	ASSERT_EQ(8u, password.length());

}*/

int main(int argc, char **argv) {
      ::testing::InitGoogleTest(&argc, argv);
      return RUN_ALL_TESTS();
}
