#ifndef CARD_HPP_
#define CARD_HPP_

class Card {
public:
	enum Colour {
		Kier = 0,
		Karo,
		Trefl,
		Pik,
		ColourEnd
	};

	enum Figure {
		FIGURE_2 = 0,
		FIGURE_3,
		FIGURE_4,
		FIGURE_5,
		FIGURE_6,
		FIGURE_7,
		FIGURE_8,
		FIGURE_9,
		FIGURE_10,
		FIGURE_J,
		FIGURE_Q,
		FIGURE_K,
		FIGURE_A,
		FigureEnd
	};

	Card()
	: mColour(ColourEnd)
	, mFigure(FigureEnd)
	, mValue(0)
	{
	}


	void setValues(Colour color, Figure figure)
	{
		mColour = color;
		mFigure = figure;
		mValue = convertFigureToValue(figure);
	}

	Colour getColour() const
	{
		return mColour;
	}

	Figure getFigure() const
	{
		return mFigure;
	}

	int getValue() const
	{
		return mValue;
	}

private:
	Colour mColour;
	Figure mFigure;
	int mValue;

	int convertFigureToValue(Figure figure)
	{
		int value = 0;

		switch(figure)
		{
		case FIGURE_2:
			value = 2 + figure;
			break;
		case FIGURE_3:
			value = 2 + figure;
			break;
		case FIGURE_4:
			value = 2 + figure;
			break;
		case FIGURE_5:
			value = 2 + figure;
			break;
		case FIGURE_6:
			value = 2 + figure;
			break;
		case FIGURE_7:
			value = 2 + figure;
			break;
		case FIGURE_8:
			value = 2 + figure;
			break;
		case FIGURE_9:
			value = 2 + figure;
			break;
		case FIGURE_10:
			value = 2 + figure;
			break;
		case FIGURE_J:
			value = 2;
			break;
		case FIGURE_Q:
			value = 3;
			break;
		case FIGURE_K:
			value = 4;
			break;
		case FIGURE_A:
			value = 11;
			break;
		default:
			value = 0;
			break;
		}

		return value;
	}
};

#endif /* CARD_HPP_ */
