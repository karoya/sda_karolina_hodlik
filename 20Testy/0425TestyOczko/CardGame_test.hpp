#include "gtest/gtest.h"
#include "Card.hpp"

TEST(CardClassTest, cardCreation)
{
	Card card;

	EXPECT_EQ(0, card.getValue());
	EXPECT_EQ(Card::ColourEnd, card.getColour());
	EXPECT_EQ(Card::FigureEnd, card.getFigure());
}
TEST(CardClassTest, cardSetValue)
{
	Card card;
	card.setValues(Card::Karo, Card::FIGURE_3);
	EXPECT_EQ(3, card.getValue());
	EXPECT_EQ(Card::Karo, card.getColour());
	EXPECT_EQ(Card::FIGURE_3, card.getFigure());
}


int main(int argc, char **argv) {
      ::testing::InitGoogleTest(&argc, argv);
      return RUN_ALL_TESTS();
}
