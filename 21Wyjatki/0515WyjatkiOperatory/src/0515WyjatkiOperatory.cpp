//============================================================================
// Name        : 0515WyjatkiOperatory.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class liczbaNieujemna
{
private:
	int mValue;
public:
	int getValue() const
	{
		return mValue;
	}

	void setValue(int value)
	{
		mValue = value;
	}

	liczbaNieujemna(int v)
	{
		if (v < 0)
		{
			throw string("k-tor: liczba jest ujemna!");
		}

		mValue = v;
	}

 	liczbaNieujemna operator- (const liczbaNieujemna& druga)
 	{
 		if (druga.getValue() > this->getValue())
 		{
 			throw string("Nie mo�na odjac. Druga liczba jest za duza");
 		}

 		return liczbaNieujemna(this->getValue() - druga.getValue());
 	}

 	liczbaNieujemna operator/ (const liczbaNieujemna& druga)
 	{
 		if (druga.getValue() == 0)
 		{
 			throw string("Dzielenie przez zero");
 		}

 		return liczbaNieujemna(druga.getValue() / this->getValue());
 	}

 	liczbaNieujemna operator* (const liczbaNieujemna& druga)
 	{
 		return liczbaNieujemna(druga.getValue() * this->getValue());
 	}

// 	void wypisz(std::ostream& stream) const
// 	{
// 		stream << mValue;
// 	}

 	friend std::ostream& operator<< (std::ostream& stream, const liczbaNieujemna& liczba);
};

std::ostream& operator<< (std::ostream& stream, const liczbaNieujemna& liczba)
{
	stream << liczba.mValue;
//	liczba.wypisz(stream);
	return stream;
}

int main()
{
	try
	{
		liczbaNieujemna duza(20);
		liczbaNieujemna mala(10);
		liczbaNieujemna zero(0);
		liczbaNieujemna jeden(1); //blad

		cout<< "mnoze " << jeden << "*" << mala << "=" << jeden * mala << endl;
		cout<< "mnoze " << duza << "*" << zero << "=" << duza * zero << endl; //blad

		try
		{
				cout << "odejmuje " << duza << "-" << mala << "=" << duza - mala << endl;
				cout << "odejmuje " << mala << "-" << duza << "=" << mala - duza << endl; //blad
		}
		catch(string& str)
		{
			cout << "odejmowanie " << str << endl;
		}

		try
		{
				cout << "dziele " << duza << "/" << mala << "=" << duza / mala	<< endl;
				cout << "dziele " << mala << "/" << zero << "=" ;
				cout << mala / zero << endl; //blad
		}
		catch(string& str)
		{
			cout << "dzielenie " << str << endl;
		}

	}
	catch (string& str)
	{
		cout << "k-tor: " << str << endl;
		throw str;
	}

	return 0;
}
