//============================================================================
// Name        : 0515WyjatkiThrow.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;
double dziel(double a, double b)
{
	std::string z("Nie mo�na dzielic przez 0!");
	if (b == 0)
	{
		throw z;
	}
	return a/b;
}
int main() {

//	dziel(4,5);
//	dziel(2,0);

	try
	{
		dziel(4,5);
		dziel(2,0);
	}
	catch(std::string& z)
	{
		std::cout<<"B��d!" << z << std::endl;
	}
	return 0;
}
