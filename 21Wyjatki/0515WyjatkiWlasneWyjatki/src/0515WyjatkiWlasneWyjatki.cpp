//============================================================================
// Name        : 0515WyjatkiWlasneWyjatki.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <exception>
#include <sstream>

using namespace std;
struct BladKonstukcji: public exception {
	int blednaLiczba;
	BladKonstukcji(int liczba) :
			blednaLiczba(liczba) {
	}
	int getBlednaWartosc()
	{
		return blednaLiczba;
	}
	virtual const char* what() const throw () {
		return "B��d w konstuktorze!";
	}
};
struct ZleArgumenty: public exception {
	int blednaLiczba;
	ZleArgumenty(int liczba) :
			blednaLiczba(liczba) {
	}
	int getBlednaWartosc()
	{
		return blednaLiczba;
	}
	virtual const char* what() const throw () {

		return "Zly Argument! Argument ";
	}
};
class liczbaNieujemna {
private:
	int mValue;
public:
	int getValue() const {
		return mValue;
	}

	void setValue(int value) {
		mValue = value;
	}

	liczbaNieujemna(int v) {
		if (v < 0) {
			throw BladKonstukcji(v);
		}

		mValue = v;
	}

	liczbaNieujemna operator-(const liczbaNieujemna& druga) {
		if (druga.getValue() > this->getValue()) {
			throw ZleArgumenty(druga.getValue());
		}

		return liczbaNieujemna(this->getValue() - druga.getValue());
	}

	liczbaNieujemna operator/(const liczbaNieujemna& druga) {
		if (druga.getValue() == 0) {
			throw ZleArgumenty(druga.getValue());
		}

		return liczbaNieujemna(druga.getValue() / this->getValue());
	}

	liczbaNieujemna operator*(const liczbaNieujemna& druga) {
		return liczbaNieujemna(druga.getValue() * this->getValue());
	}

// 	void wypisz(std::ostream& stream) const
// 	{
// 		stream << mValue;
// 	}

	friend std::ostream& operator<<(std::ostream& stream,
			const liczbaNieujemna& liczba);
};

std::ostream& operator<<(std::ostream& stream, const liczbaNieujemna& liczba) {
	stream << liczba.mValue;
//	liczba.wypisz(stream);
	return stream;
}

int main() {
	try {
		liczbaNieujemna duza(20);
		liczbaNieujemna mala(10);
		liczbaNieujemna zero(0);
		liczbaNieujemna jeden(-1); //blad

		cout << "mnoze " << jeden << "*" << mala << "=" << jeden * mala << endl;
		cout << "mnoze " << duza << "*" << zero << "=" << duza * zero << endl; //blad

		try {
			cout << "odejmuje " << duza << "-" << mala << "=" << duza - mala
					<< endl;
			cout << "odejmuje " << mala << "-" << duza << "=" << mala - duza
					<< endl; //blad
		} catch (ZleArgumenty& ex) {
			cout << "odejmowanie " << ex.what() << endl;
		}

		try {
			cout << "dziele " << duza << "/" << mala << "=" << duza / mala
					<< endl;
			cout << "dziele " << mala << "/" << zero << "=";
			cout << mala / zero << endl; //blad
		} catch (ZleArgumenty& ex) {
			cout << "dzielenie " << ex.what() << endl;
		}

	} catch (BladKonstukcji& ex) {
		cout << "k-tor: " << ex.what() << ex.getBlednaWartosc() << endl;
		//throw str;
	}

	return 0;
}
