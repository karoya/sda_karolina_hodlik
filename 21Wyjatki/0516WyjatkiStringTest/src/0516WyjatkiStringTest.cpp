//============================================================================
// Name        : 0516WyjatkiStringTest.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <exception>
#include <typeinfo>
#include <string>

using namespace std;

int main() {
	string strg1("Test");
	string strg2("ing");

	try
	{
		strg1.append(strg2, 4, 2);
	}
	catch(out_of_range& ex)
	{
		cerr<<"Niepoprawny string";
	}
	catch(exception& ex)
	{
		cerr<<"Typu " << typeid(ex).name()<<endl;
		cerr<<"Mam " << ex.what()<<endl;
	}
	cout<<strg1<<endl;

	return 0;
}
