#include <iostream>
#include <iostream>
#include <exception>

using namespace std;

//enum AircraftError
//{
//  WingsOnFire = 1,
//  WingBroken = 2,
//  NoRunway = 3,
//  Crahed = 4
//};

class AircraftException : public exception
{
public:
  AircraftException(const char* errMessage) :m_ErrMessage(errMessage){}
  // overriden what() method from exception class
  const char* what() const throw() { return m_ErrMessage; }

private:
  const char* m_ErrMessage;
};
class WingsOnFireException: public AircraftException
{
public:
	WingsOnFireException(const char* errMessage):AircraftException(errMessage)
		{

		}
};
class WingBrokenException: public AircraftException
{
public:
	WingBrokenException(const char* errMessage):AircraftException(errMessage)
		{

		}
};
class NoRunwayException: public AircraftException
{
public:
	NoRunwayException(const char* errMessage):AircraftException(errMessage)
		{

		}
};
int main() {
  try
  {
    throw AircraftException("crashed");
//    throw WingsOnFireException("pali sie");
//    throw WingBrokenException("nie ma skrzydla");
   // throw NoRunwayException("nie ma drogi");
  }
  catch (WingsOnFireException& e)
  {
    cout << e.what() << '\n';
  }
  catch (WingBrokenException& e)
   {
     cout << e.what() << '\n';
   }
  catch (NoRunwayException& e)
   {
     cout << e.what() << '\n';
   }
  catch (AircraftException& e)
   {
     cout << e.what() << '\n';
   }
//    if (e.GetError() == WingsOnFire)
//    {
//      // Fire extinguishers
//    }
//    else if (e.GetError() == WingBroken)
//    {
//      // Cannot do anything in flight - pray and rethrow
//    }
//    else if(e.GetError()== NoRunway)
//    {
//      //Call Air Traffic control to clear up runway
//    }
//    else
//    {
//      // We have crashed - throw
//      throw;
//    }
//  }

  return 0;
}
