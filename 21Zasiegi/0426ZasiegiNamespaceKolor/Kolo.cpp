/*
 * Kolo.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "Kolo.hpp"
#include <iostream>

Kolo::Kolo(int r, Kolor::Kolor kolor):
mR(r),
mPi(3.14),
mKolor(kolor)
{
	// TODO Auto-generated constructor stub

}

Kolor::Kolor Kolo::getKolor() const {
	return mKolor;
}

void Kolo::setKolor(Kolor::Kolor kolor) {
	mKolor = kolor;
}

float Kolo::getPi() const {
	return mPi;
}

void Kolo::setPi(float pi) {
	mPi = pi;
}

int Kolo::getR() const {
	return mR;
}

void Kolo::setR(int r) {
	mR = r;
}

Kolo::~Kolo() {
	// TODO Auto-generated destructor stub
}

void Kolo::wypisz()
{
	std::cout<<"Promie� to "<<getR()<<std::endl;
	std::cout<<"Pi to "<<getPi()<<std::endl;
	std::cout<<"Kolor to "<<Kolor::convertToString(getKolor())<<std::endl;
}
