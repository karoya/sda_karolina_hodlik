/*
 * Kolo.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "Figura.hpp"
#include "Kolor.hpp"

#ifndef KOLO_HPP_
#define KOLO_HPP_

class Kolo: public Figura {
private:
	int mR;
	float mPi;
	Kolor::Kolor mKolor;
public:
	Kolo(int r, Kolor::Kolor kolor);
	~Kolo();
	void wypisz();
	Kolor::Kolor getKolor() const;
	void setKolor(Kolor::Kolor kolor);
	float getPi() const;
	void setPi(float pi);
	int getR() const;
	void setR(int r);
};

#endif /* KOLO_HPP_ */
