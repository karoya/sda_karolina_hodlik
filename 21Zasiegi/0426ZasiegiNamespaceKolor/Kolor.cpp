/*
 * Kolor.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "Kolor.hpp"

std::string Kolor::convertToString(Kolor kolor)
{
	switch(kolor)
	{
	case Zielony:
		return "Zielony";
		break;
	case Fioletowy:
		return "Fioletowy";
		break;
	case Niebieski:
		return "Niebieski";
		break;
	default:
		return "Nie ma takiego koloru";
	}
}
