/*
 * Kolor.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include <string>
#ifndef KOLOR_HPP_
#define KOLOR_HPP_

namespace Kolor {
	enum Kolor
	{
		Niebieski = 0,
		Fioletowy,
		Zielony
	};
	std::string convertToString(Kolor kolor);
};

#endif /* KOLOR_HPP_ */
