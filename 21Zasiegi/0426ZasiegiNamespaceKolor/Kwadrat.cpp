/*
 * Kwadrat.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "Kwadrat.hpp"

Kwadrat::Kwadrat(int a, int b, Kolor::Kolor kolor):
mA(a),
mB(b),
mKolor(kolor)
{
	// TODO Auto-generated constructor stub

}

int Kwadrat::getA() const {
	return mA;
}

void Kwadrat::setA(int a) {
	mA = a;
}

int Kwadrat::getB() const {
	return mB;
}

void Kwadrat::setB(int b) {
	mB = b;
}

Kolor::Kolor Kwadrat::getKolor() const {
	return mKolor;
}

void Kwadrat::setKolor(Kolor::Kolor kolor) {
	mKolor = kolor;
}

Kwadrat::~Kwadrat() {
	// TODO Auto-generated destructor stub
}

void Kwadrat::wypisz()
{
	std::cout<<"Bok A to "<<getA()<<std::endl;
	std::cout<<"Bok B to "<<getB()<<std::endl;
	std::cout<<"Kolor to "<<getKolor()<<std::endl;
}
