/*
 * Kwadrat.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "Figura.hpp"
#include "Kolor.hpp"
#include <iostream>

#ifndef KWADRAT_HPP_
#define KWADRAT_HPP_

class Kwadrat: public Figura {
private:
	int mA;
	int mB;
	Kolor::Kolor mKolor;
public:
	Kwadrat(int a, int b, Kolor::Kolor kolor);
	~Kwadrat();
	void wypisz();
	int getA() const;
	void setA(int a);
	int getB() const;
	void setB(int b);
	Kolor::Kolor getKolor() const;
	void setKolor(Kolor::Kolor kolor);
};

#endif /* KWADRAT_HPP_ */
