/*
 * main.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include <iostream>
#include "Kolor.hpp"
#include "Kolo.hpp"
#include "Kwadrat.hpp"
#include "Figura.hpp"

//using namespace Kolor;

int main()
{
	std::cout<<Kolor::convertToString(Kolor::Zielony)<<std::endl;
	Kolo kolo(2, Kolor::Niebieski);
	Kwadrat kwadrat(2, 3, Kolor::Fioletowy);
	kolo.wypisz();
	kwadrat.wypisz();

	Figura* figura = 0;
	figura = &kolo;
	figura->wypisz();

	figura = &kwadrat;
	figura->wypisz();
	return 0;
}


