//============================================================================
// Name        : 0427Replikacja.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Unit.hpp"
#include "Group.hpp"
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{

	srand(time(NULL));

	cos::Group armyOne;
	cos::Unit* motherOne = new cos::Unit("1");

	//armyOne.add(motherOne);
	motherOne->addToGroup(&armyOne);

	cos::Unit* motherTwo = new cos::Unit("2");
	//armyOne.add(motherTwo);
	motherTwo->addToGroup(&armyOne);

	armyOne.printUnits();
	armyOne.replicateGroup();
	armyOne.printUnits();
	armyOne.replicateGroup();
	armyOne.printUnits();

	return 0;
}
