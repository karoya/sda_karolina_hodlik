/*
 * Group.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#include "Group.hpp"
#include "Unit.hpp"


cos::Group::Group() :
		mUnits(0), mSize(0)
{
	// TODO Auto-generated constructor stub

}

cos::Group::~Group()
{
	clear();
}
void cos::Group::resize()
{
	if (mSize == 0)
	{
		mSize += 1;
		mUnits = new Unit*[mSize];
	}
	else
	{
		mSize++;
		Unit** newUnitsArray = new Unit*[mSize];
		for (unsigned int i = 0; i < mSize - 1; i++)
		{
			newUnitsArray[i] = mUnits[i];
		}
		delete[] mUnits;

		mUnits = newUnitsArray;
	}
	//przypisac do nowej tablicy wszystkie dotychczasowe elementy
}

void cos::Group::add(Unit* unit)
{
	resize();
	mUnits[mSize - 1] = unit;
//	wola funkcje zwieksza rozmiar tablicy resize
//	ustawia wskaznik na ostatni element tablicy

}
void cos::Group::clear()
{

	for (unsigned int i = 0; i < mSize; i++)
	{
		delete mUnits[i];
	}
	delete[] mUnits;
	mSize = 0;
	mUnits = 0;
}
void cos::Group::replicateGroup()
{
	unsigned int currentSize = mSize;
	for (unsigned int i = 0; i < currentSize; i++)
	{
		mUnits[i]->replicate();
	}
}
void cos::Group::printUnits()
{
	for (unsigned int i = 0; i < mSize; i++)
	{
		mUnits[i]->printId();
	}

}
