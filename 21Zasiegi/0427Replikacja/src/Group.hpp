/*
 * Group.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include <iostream>
#ifndef GROUP_HPP_
#define GROUP_HPP_
namespace cos
{
class Unit;


class Group {
private:
	Unit** mUnits;
	unsigned int mSize;
	void resize();
public:
	Group();
	~Group();
	void add(Unit* unit);
	void clear();
	void replicateGroup();
	void printUnits();

};
};
#endif /* GROUP_HPP_ */
