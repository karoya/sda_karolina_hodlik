/*
 * Unit.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#include "Unit.hpp"
#include "Group.hpp"
#include <cstdlib>
#include <ctime>
namespace cos
{
Unit::Unit(std::string id):
mId(id), groupPtr(0)
{
	// TODO Auto-generated constructor stub

}

Unit::~Unit() {
	// TODO Auto-generated destructor stub
}

void Unit::printId()
{
	std::cout<<"Id to " << mId << std::endl;
}
void Unit::replicate()
{
	int idVariation = rand() % 10;
	char newId = '0' + idVariation;

	Unit* newborn = new Unit(mId + newId);
	newborn->addToGroup(groupPtr);
	//groupPtr->add(newborn); zamiast tego jest groupPtr->add(this);

}
void Unit::addToGroup(Group* group)
{
	groupPtr = group;
	groupPtr->add(this);
}
};
