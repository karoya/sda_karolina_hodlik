/*
 * Unit.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include <string>
#include <iostream>
#ifndef UNIT_HPP_
#define UNIT_HPP_
namespace cos
{
class Group;


class Unit {
private:
	std::string mId;
	Group* groupPtr;

public:
	Unit(std::string id);
	~Unit();
	void printId();
	void replicate();
	void addToGroup(Group*);
};
};
#endif /* UNIT_HPP_ */
