
#include <iostream>
using namespace std;

#include "Wektor.hpp"


int main() {


	Wektor w1(Punkt(3,4), Punkt(6,6));
	Wektor w2(Punkt(-4,10), Punkt(8,-7));

	w1.wypisz();
	w2.wypisz();


	Punkt pkt(3,6);
	Wektor w3(pkt, pkt*3); //3*pkt- nie zadziala
	Wektor w4(pkt, 3*pkt); //zadzia�a bo dodalismy Punkt operator w klasie Punkt
	w3.wypisz();
	w4.wypisz();



	if(w1==w2)
	{
		cout << "s� r�wne" << endl;

	}
	else
	{
		cout << "Jednak nie s� r�wne" << endl;
	}

	return 0;
}
