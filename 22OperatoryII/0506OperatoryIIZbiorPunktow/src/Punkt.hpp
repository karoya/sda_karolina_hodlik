/*
 * Punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
{
private:

	float mX;
	float mY;

public:

	Punkt();
	Punkt(float x, float y) :
			mX(x), mY(y)
	{

	}
	Punkt(const Punkt& pkt) :
			mX(pkt.getX()), mY(pkt.getY())
	{

	}

	float getX() const {
		return mX;
	}

	void setX(float x) {
		mX = x;
	}

	float getY() const {
		return mY;
	}

	void setY(float y) {
		mY = y;
	}

	Punkt operator *(const Punkt &pkt1)
	{
		Punkt pkt(0,0);
		pkt.setX(mX * pkt1.mX);
		pkt.setY(mY * pkt1.mY);
		return pkt;
	}

	Punkt operator*(const float multi) const
	{
		Punkt wynik(0, 0);
		wynik.setX(mX * multi); //mX to this->
		wynik.setY(mY * multi);

		return wynik;
	}
	Punkt operator +(const float add) const
	{
		Punkt wynik(0, 0);
		wynik.setX(mX + add); //mX to this->
		wynik.setY(mY + add);

		return wynik;
	}
	Punkt operator +(const Punkt &pkt1)
		{
			Punkt pkt(0,0);
			pkt.setX(mX + pkt1.mX);
			pkt.setY(mY + pkt1.mY);
			return pkt;
		}
	void wypisz()
	{
		std::cout << " x = " << getX() << " y = " << getY() << " ";

	}
	Punkt& operator++() // operator preinkrementacji
	{
		this->setX(mX+1);
		this->setY(mY+1);
		return *this;
	}
	Punkt operator++(int) // operator postinkrementacji
	{
		Punkt tmp(*this);

		this->operator++();

		return tmp;
	}
};
Punkt operator *(const float multi, const Punkt &pkt)
{
	return pkt * multi;
}

Punkt operator +(const float add, const Punkt &pkt)
{
	return pkt + add;
}

#endif /* PUNKT_HPP_ */
