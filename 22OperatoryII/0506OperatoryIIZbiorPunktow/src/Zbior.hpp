/*
 * Zbior.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */
#include "Punkt.hpp"
#ifndef ZBIOR_HPP_
#define ZBIOR_HPP_

class Zbior {
private:
	Punkt** mPtr;
	size_t mSize;
public:
	Zbior():
		mPtr(0), mSize(0){

	}
	~Zbior()
	{
		if (mSize != 0)
		{
			for(size_t i = 0; i<mSize; i++)
			{
				delete mPtr[i];
			}
			delete [] mPtr;
			mSize = 0;
		}
	}
	void resize()
	{
		if(mSize==0)
		{
			mPtr = new Punkt*[++mSize];
		}
		else
		{
			Punkt** tmp = new Punkt*[++mSize]; // nowa tymczasowa tablica wskaznikow do ktorej przypisujemy nowa dynamiczna tablie
			for (size_t i = 0; i<mSize; i++) // kopiowanie ze starej tablicy do nowej
			{
				tmp[i] = mPtr[i];
			}
			delete [] mPtr;
			mPtr = tmp;//podmieniamy tablice
		}


	}
	void operator +(const Punkt& pkt)
	{
		resize();
		mPtr[mSize-1] = new Punkt(pkt);
	}
	void operator +(const Zbior& zbior)
	{
		for(size_t i = 0; i < zbior.getSize(); i++)

				{
					*this + zbior[i];
				}
	}

	Punkt operator[](size_t idx) const
	{
		if (idx < mSize)
		{
			return Punkt(*mPtr[idx]);
		}
		else
		{
			return Punkt(0, 0);
		}
	}
	void wypisz()
	{
		std::cout<<"<";
		for(size_t i = 0; i < this->getSize(); i++)
		{
			std::cout<<"P"<< i;
			mPtr[i]->wypisz();
		}
		std::cout<<">"<<std::endl;
	}

	size_t getSize() const {
		return mSize;
	}

	void setSize(size_t size) {
		mSize = size;
	}

	operator bool()
		{
		return (this->getSize() != 0);
		}
};

#endif /* ZBIOR_HPP_ */
