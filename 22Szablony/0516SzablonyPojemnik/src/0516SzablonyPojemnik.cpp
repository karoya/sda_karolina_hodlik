//============================================================================
// Name        : 0516SzablonyPojemnik.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <typeinfo>

using namespace std;

template<typename T>
class Pojemnik
{
private:
	T mZmienna;
public:
	Pojemnik(T zmienna) :
			mZmienna(zmienna)
	{
	}
	T getZmienna() const
	{
		return mZmienna;
	}
	void setZmienna(T zmienna)
	{
		mZmienna = zmienna;
	}
	void wypisz()
	{
		cout << getZmienna() << endl;
	}
};
int main() {

	Pojemnik<string> pojemnikZeSznurkiem("�ubudubu");
	pojemnikZeSznurkiem.wypisz();

//	pojemnikZeSznurkiem.setZmienna(8);
	Pojemnik<int> cyfrowyPojemnik(9191);
	cyfrowyPojemnik.wypisz();
	cyfrowyPojemnik.setZmienna(111);
	cyfrowyPojemnik.wypisz();

	Pojemnik<float>* wskaznikNaPojemnik = new Pojemnik<float>(5);
	wskaznikNaPojemnik->wypisz();
	return 0;
}
