//============================================================================
// Name        : 0516SzablonyPrzyklady.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <typeinfo>

using namespace std;

template<int N, typename T>
void wypisz(T zmienna)
{
	for(int i = 0; i < N;i++)
		{
			cout<< zmienna;
		}
	cout<<endl;
}

int main() {

	string test = "zz ";

	wypisz<10, string>(test);
	wypisz<20, int>(7);
	wypisz<3, float>(7);
	return 0;
}
