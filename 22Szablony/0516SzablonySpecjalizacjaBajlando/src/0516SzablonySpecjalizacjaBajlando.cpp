//============================================================================
// Name        : 0516SzablonySpecjalizacjaBajlando.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <typeinfo>
using namespace std;

template<int N, typename T>
void wypisz(T zmienna)
{
	for(int i = 0; i < N;i++)
		{
			cout<< zmienna;
		}
	cout<<endl;
}
template<>
void wypisz<10, string>(string zmienna)
{
	cout<<"Bajlando!"<<endl;
}

int main() {

	string test = "zz ";

	wypisz<10, string>(test);
	wypisz<20, int>(7);
	wypisz<3, float>(7);
	return 0;
}
