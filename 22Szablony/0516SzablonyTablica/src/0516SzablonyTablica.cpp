//============================================================================
// Name        : 0516SzablonyTablica.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <typeinfo>
#include <exception>
#include <cstdlib>
#include <ctime>
using namespace std;

template<typename T>
class Tablica
{
private:
	T* mZmienna;
	size_t mSize;
	void resize()
	{
		if(mSize == 0)
		{
			mSize++;
			mZmienna = new T[mSize];
		}
		else
		{
			mSize++;
			T* tmpZmienna = new T[mSize];

			for (unsigned int i = 0; i<mSize-1; i++)
			{
				tmpZmienna[i] = mZmienna[i];
			}
			delete[] mZmienna;
			mZmienna = tmpZmienna;
		}

	}
public:
	Tablica():
		mZmienna(0), mSize(0)
	{

	}
	~Tablica()
	{

	}
	void add(const T& zmienna)
	{
		resize();
		mZmienna[mSize - 1] = T(zmienna);
	}
	size_t size()
	{
		return mSize;
	}
	int find(const T& zmienna)
	{
		for (size_t idx=0; idx < mSize; idx++)
		{
			if(get(idx) == zmienna)
			{
				return idx;
			}
		}
		return -1;
	}

//	int size()
//	{
//
//	}
	bool empty()
	{
		return ((mSize == 0) ? true : false);
	}
//	void remove(const T& elem)
//	{
//		int pos = find(elem);
//		if(pos> -1)
//		{
//			remove(pos);
//		}
//		else
//		{
//			//Nic throw(out_of_range);
//		}
//	}
	void remove(unsigned int pos)
	{
		if (pos<mSize && pos>0)
		{
			T* tmpZmienna = new T[mSize -1];
			for(unsigned int i=0; i<mSize-1; i++)
			{
				if(i<pos)
				{
					tmpZmienna[i] = mZmienna[1];
				}
				else if(i>=pos)
				{
					tmpZmienna[i] = mZmienna[i+1];
				}
			}
			delete[] mZmienna;
			mZmienna = tmpZmienna;
			mSize--;
		}
		else
		{
			throw out_of_range("Indeks poza zakresem");
		}
	}
	T get(unsigned int pos)
	{
		if (pos<mSize && pos>=0)
		{
		return mZmienna[pos];
		}
		else
		{
			throw out_of_range("Indeks poza zakresem");
		}
	}
	T operator[](size_t pos)
	{
		return get(pos);
	}
	void clear()
	{
		delete[] mZmienna;
		mSize = 0;
		mZmienna = 0;
	}
};
int main() {

//	Tablica<int> tabl;
//
//	tabl.add(3);
//	tabl.add(20);
//	tabl.add(33);
//
//	cout<< tabl.get(0)<< " " << tabl[1] << tabl.get(2) <<endl;
//
//	tabl.remove(20);
//	cout<< tabl.get(0)<< " " << tabl[1] <<endl;

	Tablica<string> tabl;

	tabl.add("kot");
	tabl.add("pies");
	tabl.add("zupa");

	cout<< tabl.get(0)<< " " << tabl[1] << tabl.get(2) <<endl;

	tabl.remove(1);
	cout<< tabl.get(0)<< " " << tabl[1] <<endl;
	return 0;
}
