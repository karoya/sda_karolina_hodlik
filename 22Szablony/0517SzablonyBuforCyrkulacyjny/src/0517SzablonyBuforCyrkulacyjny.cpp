//============================================================================
// Name        : 0517SzablonyBuforCyrkulacyjny.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <exception>
using namespace std;

template<typename T>
class CircularBuffer
{
private:
	struct Node
		{
			T value;
			Node* next;

			Node(T value)
			{
				this->value = value;
				next = 0;
			}

		};

	Node* readPtr;
	Node* addPtr;
	size_t mSize;
	size_t mMaxSize;

public:
	CircularBuffer(size_t maxSize)
	: readPtr(0)
	, addPtr(0)
	, mSize(0)
	, mMaxSize(maxSize)
	{}

	void add(const T& elem)
	{
		if (mSize == 0)
		{
			Node* tmpPtr = new Node(elem);
			addPtr = tmpPtr;
			readPtr = tmpPtr;
			tmpPtr->next = tmpPtr;
			mSize++;
		}
		else if (mSize < mMaxSize)
		{
			Node* tmpPtr = new Node(elem);
			tmpPtr->next = addPtr->next;
			addPtr->next = tmpPtr;
			addPtr = tmpPtr;
			mSize++;
		}
		else if (mSize == mMaxSize)
		{
			addPtr = addPtr->next;
			addPtr->value = elem;
		}
		else
		{
			//pomyslec o obsludze bledy - jezeli size > max size
		}
		}

	void clear();

	size_t maxSize()
	{
		return mMaxSize;
	}
	size_t size()
	{
		return mSize;
	}

	T& next()
	{
		if(readPtr == 0)
		{
	//		throw out_of_range();
		}
		else
		{
			readPtr = readPtr->next;//Poprawiic aby odczytywal pierwszy element od razu
			return readPtr->value;
		}
	}

};
int main() {

	CircularBuffer<int> c(3);

	c.add(10);
	c.add(20);
	c.add(30);
	c.add(40);
	cout << c.next()<<endl;
	cout << c.next()<<endl;
	cout << c.next()<<endl;
	cout << c.next()<<endl;
	cout << c.next()<<endl;
	cout << c.next()<<endl;
	cout << c.next()<<endl;
	cout << c.next()<<endl;
	cout << c.next()<<endl;

	return 0;
}
