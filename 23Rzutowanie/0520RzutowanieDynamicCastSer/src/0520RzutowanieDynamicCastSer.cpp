//============================================================================
// Name        : 0520RzutowanieDynamicCastSer.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

class Ser {
private:
	double mCena;
public:
	Ser(double cena) :
			mCena(cena) {

	}
	virtual ~Ser()
	{

	}
	virtual void podajCene() {
		cout << "Cena sera: " << mCena << endl;
	}

	double getCena() const {
		return mCena;
	}

	void setCena(double cena) {
		mCena = cena;
	}
};

class SerZolty: public Ser {
private:
	int mWiek;
public:
	SerZolty(double cena, int wiek) :
			Ser(cena), mWiek(wiek) {

	}
//	void podajCene()
//	{
//		cout << "Cena zoltego sera: " << getCena() << endl;
//	}
	void podajWiek() {
		cout << "Wiek zoltego sera: " << getWiek() << endl;
	}

	int getWiek() const {
		return mWiek;
	}

	void setWiek(int wiek) {
		mWiek = wiek;
	}
};

class SerBialy: public Ser {

public:

	enum Fat {
		poltlusty, tlusty, chudy
	};
	SerBialy(double cena, Fat fat) :
			Ser(cena), mFat(fat) {

	}
	void podajRodzaj() {
		cout << "Rodzaj bialego sera: " << wypiszRodzaj() << endl;
	}
	void podajCene() {
		cout << "Cena bialego sera: " << getCena() << endl;
	}

	Fat getFat() const {
		return mFat;
	}

	void setFat(Fat fat) {
		mFat = fat;
	}

private:
	Fat mFat;
	string wypiszRodzaj() {
		switch (mFat) {
		case poltlusty:
			return "P�t�usty";
			break;
		case tlusty:
			return "T�usty";
			break;
		case chudy:
			return "Chudy";
			break;
		}
		return "a";
	}
};

void sortuj(Ser** tab, int rozmiar) {
	SerBialy** bialaTab = new SerBialy*[rozmiar];
	SerZolty** zoltaTab = new SerZolty*[rozmiar];
	int ileBialych = 0;
	int ileZoltych = 0;
	SerBialy* bialyTmp = 0;
	SerZolty* zoltyTmp = 0;

	for(int i = 0; i < rozmiar; i++)
	{
		bialyTmp = dynamic_cast<SerBialy*>(tab[i]);
		if(bialyTmp)
		{
			bialaTab[ileBialych] = bialyTmp;
			ileBialych++;
			continue;
		}
		zoltyTmp = dynamic_cast<SerZolty*>(tab[i]);
		if (zoltyTmp)
		{
			zoltaTab[ileZoltych] = zoltyTmp;
			ileZoltych++;
		}

	}

	for(int i = 0; i < ileBialych; i++)
	{
		cout << "Bialy! ";
		bialaTab[i]->podajCene();
		bialaTab[i]->podajRodzaj();
		cout << endl;
	}

	for(int i = 0; i < ileZoltych; i++)
	{
		cout << "Zolty! ";
		zoltaTab[i]->podajCene();
		zoltaTab[i]->podajWiek();
		cout << endl;
	}
	delete[] bialaTab;
	delete[] zoltaTab;
}

int main() {

	Ser* sery[8];

	sery[0] = new SerBialy(10, SerBialy::chudy);
	sery[1] = new SerZolty(301, 23);
	sery[2] = new SerZolty(10, 1);
	sery[3] = new SerBialy(8, SerBialy::tlusty);
	sery[4] = new SerBialy(9, SerBialy::tlusty);
	sery[5] = new SerBialy(15, SerBialy::poltlusty);
	sery[6] = new SerBialy(13, SerBialy::chudy);
	sery[7] = new SerZolty(2000, 33);

	sortuj(sery, 8);

	sery[0]->podajCene();

	for(int i =0; i<8;i++)
	{
		delete sery[i];
	}
	return 0;
}
