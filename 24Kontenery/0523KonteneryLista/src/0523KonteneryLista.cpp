//============================================================================
// Name        : 0523KonteneryLista.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
using namespace std;

bool sortowanieIntowMalejace(const int& first, const int& second)
{
	return first>second;
}

int main() {

	list<int> listI;

//	listI.push_back(7);
//	listI.push_back(23);
//	listI.push_back(77);
//	listI.push_back(4);
//	listI.push_back(53);
//	listI.push_back(15);
//	listI.push_back(9);
//	listI.push_front(5);
	//listI.insert(2, 99);
	listI.push_back(3);
	listI.push_back(0);
	listI.push_back(4);
	listI.push_back(4);
	listI.push_back(2);
	listI.push_back(3);
	listI.push_back(5);
	listI.push_front(5);

	list<int> listI2;
	listI2.push_back(11);
	listI2.push_front(12);
	for (list<int>::iterator it = listI.begin(); it != listI.end(); it++)
	{
		cout<<*it<<" ";
	}
	cout<<endl;
	for (list<int>::reverse_iterator it = listI.rbegin(); it != listI.rend(); it++)
	{
		cout<<*it<<" ";
	}
	cout<<endl;
	for (list<int>::iterator it = listI2.begin(); it != listI2.end(); it++)
	{
		cout<<*it<<" ";
	}
	cout<<endl;

	listI2.splice(listI2.begin(), listI);
	for (list<int>::iterator it = listI2.begin(); it != listI2.end(); it++)
	{
		cout<<*it<<" ";
	}
	cout<<endl;
	listI2.splice(listI2.begin(), listI, ++listI.begin(), --listI.end());
	for (list<int>::iterator it = listI2.begin(); it != listI2.end(); it++)
	{
		cout<<*it<<" ";
	}
	cout<<endl;
	bool czyPalindrom = true;
	list<int>::iterator a = listI.begin();
	list<int>::reverse_iterator b = listI.rbegin();
	while(a !=listI.end() || b != listI.rend())
	{
		if (*a!=*b)
		{
			czyPalindrom = false;
			break;
		}
		++a;
		++b;
	}
	cout<<(czyPalindrom ? " " :"nie ") << "jest palindromem" << endl;
	listI.sort(sortowanieIntowMalejace);
	for (list<int>::iterator it = listI.begin(); it != listI.end(); it++)
	{
		cout<<*it<<" ";
	}
	cout<<endl;

	return 0;
	}


