//============================================================================
// Name        : 0523KonteneryListaStringow.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
using namespace std;

bool sortowanieIntowMalejace(const int& first, const int& second)
{
	return first>second;
}

bool stringPoZnakach(const string& first, const string& second)
{
	return first.size()<second.size();
}


int main() {

	list<string> listStr;

	listStr.push_back("Ala");
	listStr.push_back("ala");
	listStr.push_back("Ma");
	listStr.push_back("ma");
	listStr.push_back("Kota");
	listStr.push_back("kota");
	listStr.push_back("tak!");
	listStr.push_front("Szalona");

	for (list<string>::iterator it = listStr.begin(); it != listStr.end(); it++)
	{
		cout<<*it<<" ";
	}
	cout<<endl;
	for (list<string>::reverse_iterator it = listStr.rbegin(); it != listStr.rend(); it++)
	{
		cout<<*it<<" ";
	}
	cout<<endl;
	bool czyPalindrom = true;
	list<string>::iterator a = listStr.begin();
	list<string>::reverse_iterator b = listStr.rbegin();
	while(a !=listStr.end() || b != listStr.rend())
	{
		if (*a!=*b)
		{
			czyPalindrom = false;
			break;
		}
		++a;
		++b;
	}
	cout<<(czyPalindrom ? " " :"nie ") << "jest palindromem" << endl;
	listStr.sort(stringPoZnakach);
	for (list<string>::iterator it = listStr.begin(); it != listStr.end(); it++)
	{
		cout<<*it<<" ";
	}
	cout<<endl;

	return 0;
	}
