//============================================================================
// Name        : 0523KonteneryVector.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main() {

	vector<int> vectInt(100, 8); // 100 elementow o wartosci 8
	vector<string> vectStr;

	vectStr.push_back("Lala");
	vectStr.push_back("dasdsad");
	vectStr.push_back("Lhhgfhf");
	vectStr.push_back("aaaaaaaaaaaaaaaa");
	vectStr.push_back("ssss");
	vectStr.push_back("tttttt");
	vectStr.push_back("nnnnnnnn");

	cout << vectInt.capacity()<<"; "<< vectInt.size()<<"; "<< vectInt.max_size()<<endl;
	vectInt.reserve(1000);
	cout << vectInt.capacity()<<"; "<< vectInt.size()<<"; "<< vectInt.max_size()<<endl;
	vectInt.resize(1300,12);
	cout << vectInt.capacity()<<"; "<< vectInt.size()<<"; "<< vectInt.max_size()<<endl;


	for(int i = 0; i<vectInt.size(); i++)
	{
		cout<<vectInt[i]<<" " ;
	}
	cout<<endl;
	while(!vectInt.empty())
	{
		cout<<vectInt.back();
		vectInt.pop_back();
	}
	for(int i = 0; i<vectInt.size(); i++)
		{
			cout<<vectInt[i]<<" " ;
		}
	cout<<endl;
	for(int i = 0; i<vectStr.size(); i++)
	{
		cout<<vectStr.at(i)<<" " ;
	}
	cout<<endl;
	return 0;
}
