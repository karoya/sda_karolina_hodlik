//============================================================================
// Name        : 0524KontenerySet.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <set>
#include <string>

using namespace std;

struct compareInrefralPart
{
	bool operator() (float first, float second) const
	{
		return static_cast<int> (first) < static_cast<int>(second);
	}

};

int main() {

	set<string> imiona;

	string tabString[] = {"Afryka", "Azja", "Europa"};

	imiona.insert(tabString, tabString+3);

	imiona.insert("Lala");
	imiona.insert("Mol");
	imiona.insert("Lucu�");
	imiona.insert("Zofia");
	imiona.insert("Czacha");
	imiona.insert("Europa"); // jest ignorowane bo taka wartosc jest juz dodana

	for(set<string>::iterator it = imiona.begin(); it!=imiona.end();it++)
	{
		cout<<*it<<endl;
	}

	set<float, compareInrefralPart> liczby; // unikatowe czesci calkowite  @2.1 2.2 -> ma byc

	liczby.key_comp();

	liczby.insert(2.1);
	liczby.insert(3.5);
	liczby.insert(24.1);
	liczby.insert(2.2);


	for(set<float>::iterator it = liczby.begin(); it!=liczby.end();it++)
		{
			cout<<*it<<endl;
		}

	set<float>::iterator it;
	it = liczby.find(3.5);
	liczby.erase(it);

	cout<<endl;
	for(set<float>::iterator it = liczby.begin(); it!=liczby.end();it++)
		{
			cout<<*it<<endl;
		}
	return 0;
}
