//============================================================================
// Name        : 0525KonteneryForEach.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
#include <algorithm>
#include <deque>
#include <iterator>
#include <ostream>
using namespace std;
void print(float x)
{
	cout << x << " ";
}
void castInteferPart(float& x)
{
	x = static_cast<int>(x);
}
struct Sum // funktor, ktory przechowuje jakas wartosc
{
	Sum():sum(0){}
	void operator() (float x)
	{
		sum += x; // moze liczyc np. srednia
	}
	float sum;
};

int main() {

	list<float> num;
	num.push_back(33.123);
	num.push_back(2.1);
	num.push_back(4.0);
	num.push_back(15.31);
	num.push_back(9.12);
	num.push_back(10.2);

	for_each(num.begin(), num.end(), print);
	cout<<endl;
	for_each(num.begin(), num.end(), castInteferPart);
	for_each(num.begin(), num.end(), print);

	Sum suma = for_each(num.begin(), num.end(), Sum());
	cout<<"Suma= "<<suma.sum<<endl;

	return 0;
}
