//============================================================================
// Name        : 0525KonteneryIteratorVector.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
#include <deque>
#include <iterator>
#include <ostream>
using namespace std;

int main() {

	deque<int> c;
	vector<int> d;

	for(int i = 1; i<6; i++)
	{
		c.push_back(i);
		d.push_back(i*10);
	}

	//front_insert_iterator< deque<int> > frontIt(c);
	//back_insert_iterator< deque<int> > backIt(c);

	//copy(d.rbegin(), d.rend(), frontIt);
	//copy(d.begin(), d.end(), backIt); // copy(d.begin(), d.end(), back_inserter(c) - zrobi to samo, zwroci obiekt back insert interetor na typ t
	copy(d.begin(), d.end(), back_inserter(c));
	for(deque<int>::iterator it = c.begin(); it!=c.end();advance(it, 1))
	{
		cout<<*it<<" " ;
	}
	cout<<endl;
	for(unsigned int i = 0; i<c.size(); i++)
	{
		cout<<c[i]<<" " ;
	}
	cout<<endl;
	std::copy(c.begin(), c.end(), std::ostream_iterator<int>(std::cout, " ")); // wypisuje
	return 0;
}
