//============================================================================
// Name        : 0527KonteneryZad9Wielokrotnosci23.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
bool podzielne2(int x)
{
	return (x%2!=0);
}
bool podzielne3(int x)
{
	return (x%3!=0);
}

void podzielne(int b) {
	vector<int> liczby;

	for (int a = 1; a <= b; a++)
	{
		liczby.push_back(a);
	}
	vector<int> liczbyDwa(liczby.size());
	vector<int> liczbyTrzy(liczby.size());
	vector<int> porownanieLiczb(liczby.size());
//	liczbyDwa.reserve(liczby.size());
//	liczbyTrzy.reserve(liczby.size());
	vector<int>::iterator it;
	it = remove_copy_if(liczby.begin(),liczby.end(), liczbyDwa.begin(), podzielne2);
	liczbyDwa.resize(distance(liczbyDwa.begin(), it));

	it = remove_copy_if(liczby.begin(),liczby.end(), liczbyTrzy.begin(), podzielne3);
	liczbyTrzy.resize(distance(liczbyTrzy.begin(), it));

	cout<<"Podzielne przez 2:";
	for(vector<int>::iterator it = liczbyDwa.begin(); it!=liczbyDwa.end();++it)
	{
		cout<<*it<<" " ;
	}
	cout<<endl;
	cout<<"Podzielne przez 3:";
	for(vector<int>::iterator it = liczbyTrzy.begin(); it!=liczbyTrzy.end();++it)
	{
		cout<<*it<<" " ;
	}
	cout<<endl;
	vector<int>::iterator pos;
	pos = set_intersection(liczbyDwa.begin(), liczbyDwa.end(), liczbyTrzy.begin(), liczbyTrzy.end(), porownanieLiczb.begin());
	porownanieLiczb.resize(distance(porownanieLiczb.begin(), pos));
	for(vector<int>::iterator it = porownanieLiczb.begin(); it!=porownanieLiczb.end();++it)
	{
		cout<<*it<<" " ;
	}
}
int main() {

	podzielne(20);
	cout<<endl;
	podzielne(30);

	return 0;
}

