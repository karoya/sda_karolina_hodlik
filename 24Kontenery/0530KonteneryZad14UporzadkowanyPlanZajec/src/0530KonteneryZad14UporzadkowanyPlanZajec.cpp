//============================================================================
// Name        : 0530KonteneryZad14UporzadkowanyPlanZajec.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
//14. Wczytaj dane z pliku z list� kurs�w i ich dni/godzin
//    Format: [nazwa] [dzien] [poczatek] [koniec]
//    a)Wypisz podany plan uszeregowany wed�ug dnia tygodnia a nast�pnie godziny rozpocz�cia
//    b)Wypisz wszystkie kursy, kt�re maj� ze sob� konflikt godzin.
#include <iostream>
#include <ostream>
#include <fstream>
#include <utility>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;
class Zajecia
{
	string mNazwa;
	int mData;
	int mPoczatek;
	int mKoniec;
public:
	Zajecia():mNazwa(0), mData(0), mPoczatek(0), mKoniec(0)
	{

	}
	Zajecia(string nazwa, int data, int poczatek, int koniec)
	: mNazwa(nazwa)
	, mData(data)
	, mPoczatek(poczatek)
	, mKoniec(koniec)
	{


	}
	bool operator<(Zajecia& drugi)
	{
		if(this->mData==drugi.mData)
		{
			return this->mPoczatek<drugi.mPoczatek;

		}
		else
		{
			return this->mData<drugi.mData;
		}
	}
	void pokaz ()
	{
		cout<<"["<<mNazwa<<"]["<<mData<<"]["<<mPoczatek<<"]["<<mKoniec<<"]"<<endl;
	}

	int getData() const {
		return mData;
	}

	int getPoczatek() const {
		return mPoczatek;
	}

	int getKoniec() const {
		return mKoniec;
	}
};

int main() {

	fstream plik;
	plik.open("kursy.dat", ios::in);
	if(!plik.good())
	{
		cout<<"Nie udalo sie otworzyc pliku!\nKoniec programu!"<<endl;
		return 1;
	}

	vector<Zajecia> listaZajec;

	string nazwa;
	int data;
	int poczatek;
	int koniec;

	while(!plik.eof())
	{
		plik>>nazwa>>data>>poczatek>>koniec;
		listaZajec.push_back(Zajecia(nazwa, data, poczatek, koniec));
	}
	sort(listaZajec.begin(),listaZajec.end());
	for(vector<Zajecia>::iterator it=listaZajec.begin(); it != listaZajec.end(); it++)
	{
		for(vector<Zajecia>::iterator its = listaZajec.begin()+1; its!=listaZajec.end(); its++)
		{
		if(it!=its && it->getData()==its->getData() && it->getPoczatek()>its->getPoczatek() && it->getPoczatek()<its->getKoniec())
		{
			it->pokaz();
			its->pokaz();
		}
		}
	}
//	for(vector<Zajecia>::iterator it=listaZajec.begin(); it != listaZajec.end(); it++)
//	{
//		it->pokaz();
//	}

	plik.close();



	return 0;
}
