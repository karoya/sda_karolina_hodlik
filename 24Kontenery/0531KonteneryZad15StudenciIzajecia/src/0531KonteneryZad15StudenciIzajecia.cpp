//============================================================================
// Name        : 0531KonteneryZad15StudenciIzajecia.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//============================================================================
// Name        : 0530KonteneryZad14UporzadkowanyPlanZajec.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
//15. Do poprzedniego zadania dodaj numer sali oraz drugi plik z list� student�w i ich kursami na ktore sie zapisali:
//    Format: [imie_Nazwisko] [liczba_kursow] [kurs1] [kurs2] .... [kursN]
//    a) Wyszukaj i wypisz wszystkie konfliktuj�ce kursy danego studenta
//    b) Wylicz ilu studentow uczeszcza na dany kurs (map)
//    c) Dodaj usuwanie zduplikowanych kursow
#include <iostream>
#include <ostream>
#include <fstream>
#include <utility>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Zajecia
{
	string mNazwa;
	int mData;
	int mPoczatek;
	int mKoniec;
	int mSala;
public:
	Zajecia():mNazwa(""), mData(0), mPoczatek(0), mKoniec(0), mSala(0) {}
	Zajecia(string nazwa, int data, int poczatek, int koniec, int sala)
	: mNazwa(nazwa)
	, mData(data)
	, mPoczatek(poczatek)
	, mKoniec(koniec)
	, mSala(sala) {}
	Zajecia(string nazwa): mNazwa(nazwa), mData(0), mPoczatek(0), mKoniec(0), mSala(0)
	{

	}
	bool operator<(Zajecia& drugi)
	{
		if(this->mData==drugi.mData)
		{
			return this->mPoczatek<drugi.mPoczatek;
		}
		else
		{
			return this->mData<drugi.mData;
		}
	}
	void pokaz ()
	{
		cout<<"["<<mNazwa<<"]["<<mData<<"]["<<mPoczatek<<"]["<<mKoniec<<"]"<<endl;
	}
	int getData() const {
		return mData;
	}
	int getPoczatek() const {
		return mPoczatek;
	}
	int getKoniec() const {
		return mKoniec;
	}
};
class Student: public Zajecia
{
	string mImieInazwisko;
	vector<string> mKursy;
public:
	Student(): 	mImieInazwisko(""), mKursy(0){}
	Student(string imieInazwisko,  vector<string> kursy):mImieInazwisko(imieInazwisko), mKursy(kursy){}
	void wypisz()
		{
			cout<<"["<<mImieInazwisko<<"]";
			cout<<"["<<mKursy.size()<<"]";
			for(vector<string>::iterator it = mKursy.begin(); it!=mKursy.end(); ++it)
			{

			cout<<"["<<*it<<"]";
			}
		cout<<endl;
		}

};
int main() {

	fstream plik;
	plik.open("kursy.dat", ios::in);
	if(!plik.good())
	{
		cout<<"Nie udalo sie otworzyc pliku!\nKoniec programu!"<<endl;
		return 1;
	}
	vector<Zajecia> listaZajec;
	string nazwa;
	int data;
	int poczatek;
	int koniec;
	int sala;
	while(!plik.eof())
	{
		plik>>nazwa>>data>>poczatek>>koniec>>sala;
		listaZajec.push_back(Zajecia(nazwa, data, poczatek, koniec, sala));
	}
	fstream plikStudentow;
	plikStudentow.open("studenci.dat");
	if(!plikStudentow.good())
	{
		cout<<"Nie udalo sie otworzyc pliku!\nKoniec programu!"<<endl;
		return 1;
	}

	vector<Student> listaStudentow;
	string imieInazwisko;
	int iloscZajec;
	vector<string> kursy;
	string kurs;

	while(!plikStudentow.eof())
		{
			plikStudentow>>imieInazwisko>>iloscZajec;
			for(int i = 1; i <=iloscZajec; i++)
			{
			plikStudentow>>kurs;
			kursy.push_back(kurs);
			}
			listaStudentow.push_back(Student(imieInazwisko, kursy));

			kursy.clear();
		}
	for(vector<Student>::iterator it=listaStudentow.begin(); it != listaStudentow.end(); it++)
	{
		it->wypisz();

	}


//	sort(listaZajec.begin(),listaZajec.end());
//	for(vector<Zajecia>::iterator it=listaZajec.begin(); it != listaZajec.end(); it++)
//	{
//		for(vector<Zajecia>::iterator its = listaZajec.begin()+1; its!=listaZajec.end(); its++)
//		{
//		if(it!=its && it->getData()==its->getData() && it->getPoczatek()>its->getPoczatek() && it->getPoczatek()<its->getKoniec())
//		{
//			it->pokaz();
//			its->pokaz();
//		}
//		}
//	}
//	for(vector<Zajecia>::iterator it=listaZajec.begin(); it != listaZajec.end(); it++)
//	{
//		it->pokaz();
//	}

	plik.close();



	return 0;
}
