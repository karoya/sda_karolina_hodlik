//============================================================================
// Name        : 0601KonteneryZad22Animal.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
//22. Klasa NIEabstrakcyjna bazowa Animal (przechowuj�ca inta - numer seryjny) + dwie klasy pochodne Dog, Bird przeci�zaj�ce wirtualna metod� whoAreYou() -> wypisujaca nazw� klasy.
//    Stworzy� kontener na klas� Animal i wype�ni� go losowo 200 obiektami Dog lub Bird nadaj�c im kolejne numery seryjne. Nast�pnie wywo�a� na ka�dym z element�w metod� whoAreYou.
//    Rodzieli� Dog od Bird na dwia osobne kontenery i ponownie wywolac metod� whoAreYou().
#include "Animal.hpp"
#include "Dog.hpp"
#include "Bird.hpp"
#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <algorithm>
using namespace std;

int main() {

	srand( time( NULL ) );

	vector<Animal*> zwierzeta;
	int z = 1;

	for (int i = 0; i < 200; ++i)
	{
		int x = rand() % 2;
		if(x == 0)
		{
			zwierzeta.push_back(new Bird(z));
			z++;
		}
		else if(x == 1)
		{
			zwierzeta.push_back(new Dog(z));
			z++;
		}

	}
//	for(vector<Animal*>::iterator it = zwierzeta.begin(); it!=zwierzeta.end(); ++it)
//	{
//		(*it)->whoAreYou();
//	}

	Dog* dogPtr;
	Bird* birdPtr;

	vector<Dog> dogs;
	vector<Bird> birds;

	for(vector<Animal*>::iterator it = zwierzeta.begin(); it!=zwierzeta.end(); ++it)
	{
		dogPtr = dynamic_cast<Dog*>(*it);

		if(dogPtr)
		{
			dogs.push_back(*dogPtr);
		}
		else
		{
			birdPtr = dynamic_cast<Bird*>(*it);
			if(birdPtr)
			{
				birds.push_back(*birdPtr);
			}
		}
	}
	for(vector<Animal*>::iterator it = zwierzeta.begin(); it!=zwierzeta.end(); ++it)
	{
		delete *it;
	}
	for(vector<Dog>::iterator it = dogs.begin(); it!=dogs.end(); ++it)
	{
		it->whoAreYou();
	}
	for(vector<Bird>::iterator it = birds.begin(); it!=birds.end(); ++it)
	{
		it->whoAreYou();
	}

	return 0;
}
