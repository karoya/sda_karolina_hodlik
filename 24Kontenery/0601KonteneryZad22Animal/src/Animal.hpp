/*
 * Animal.hpp
 *
 *  Created on: 01.06.2017
 *      Author: RENT
 */
#include <iostream>
using namespace std;
#ifndef ANIMAL_HPP_
#define ANIMAL_HPP_

class Animal
{
private:
	int mNumerSeryjny;

public:
	Animal(int numer): mNumerSeryjny(numer){}
	virtual ~Animal(){}
	virtual void whoAreYou()
	{
		cout<<"Animal"<<" ";
		cout<<mNumerSeryjny<<endl;
	}
	int getNumerSeryjny() const {
		return mNumerSeryjny;
	}

};



#endif /* ANIMAL_HPP_ */
