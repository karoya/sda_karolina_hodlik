/*
 * Dog.hpp
 *
 *  Created on: 01.06.2017
 *      Author: RENT
 */
#include "Animal.hpp"
#include <iostream>
using namespace std;

#ifndef DOG_HPP_
#define DOG_HPP_

class Dog: public Animal {

public:
	Dog(int numer): Animal(numer){}
	~Dog(){}
	void whoAreYou()
	{
		cout<<"Dog"<<" ";
		cout<<getNumerSeryjny()<<endl;
	}
};

#endif /* DOG_HPP_ */
